#include "driver_comm.h"

#undef DRV_NAME
#define DRV_NAME "DRV_FLASH"

static HANDLE testTask;

static void demo_flash_ls(char *dirName)
{
    AMOPENAT_FS_FIND_DATA findResult;
    INT32 iFd = -1;

    iFd = iot_fs_find_first(dirName, &findResult);


	DRV_PRINT("[fs] %s ls:", dirName);
    DRV_PRINT("[fs] \t%s:\t%s\t%d\t%d\t", ((findResult.st_mode&E_FS_ATTR_ARCHIVE)? "FILE":"DIR"),    
                                                findResult.st_name,
                                                findResult.st_size,
                                                findResult.mtime);

    while(iot_fs_find_next(iFd, &findResult) == 0)
    {
        DRV_PRINT("[fs] \t%s:\t%s\t%d\t%d\t", ((findResult.st_mode&E_FS_ATTR_ARCHIVE)? "FILE":"DIR"),    
                                                findResult.st_name,
                                                findResult.st_size,
                                                findResult.mtime);
    }

    if(iFd >= 0)
    {
        iot_fs_find_close(iFd);
    }
    
}

static void prvTest(void)
{
	T_AMOPENAT_USER_FSMOUNT param;
	BOOL ret;
	
	iot_os_sleep(100);
	
	/*0-4M创建文件系统 /ext1 */
	param.exFlash = E_AMOPENAT_FLASH_EXTERN_PINGPIO;
	param.offset = 0;
	param.size = 0x1000000;
	param.path = "/ext1";
	param.clkDiv = 4; 
	ret = iot_fs_mount(&param);
	if (!ret)
	{
		DRV_PRINT_RESULT(iot_fs_format(&param));	
		DRV_PRINT_RESULT(iot_fs_mount(&param));
	}
	demo_flash_ls("/ext1");
	iot_fs_make_dir("/ext1/dir_test", 0);
	DRV_PRINT_RESULT((ret = iot_fs_open_file("/ext1", FS_O_RDWR | FS_O_CREAT | FS_O_TRUNC)));
	DRV_PRINT_RESULT(iot_fs_close_file(ret));
	DRV_PRINT_RESULT(iot_fs_unmount(&param));
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		prvTest();
		
		iot_os_sleep(5000);
	}
}

void pub_enable_flash(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "flash");
}
