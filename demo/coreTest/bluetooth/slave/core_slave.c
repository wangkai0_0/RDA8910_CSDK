#include "string.h"
#include "iot_debug.h"
#include "iot_bluetooth.h"
#include "driver_comm.h"

#undef DRV_NAME
#define DRV_NAME "DRV_SLAVE"

#define BT_8910_TP_UUID 0xFEE0
#define BT_8910_TP_UUID_CHAR 0xFEE1
#define BT_8910_FEEDBACK_CHAR 0xFEE2

//GATT Characteristic attribute
#define ATT_CHARA_PROP_BROADCAST           0x01
#define ATT_CHARA_PROP_READ         0x02
#define ATT_CHARA_PROP_WWP          0x04    // WWP short for "write without response"
#define ATT_CHARA_PROP_WRITE               0x08
#define ATT_CHARA_PROP_NOTIFY              0x10
#define ATT_CHARA_PROP_INDICATE     0x20
#define ATT_CHARA_PROP_ASW          0x40    // ASW short for "Authenticated signed write"
#define ATT_CHARA_PROP_EX_PROP      0x80

#define ATT_PM_READABLE                    0x0001
#define ATT_PM_WRITEABLE                   0x0002
#define ATT_PM_R_AUTHENT_REQUIRED          0x0004
#define ATT_PM_R_AUTHORIZE_REQUIRED        0x0008
#define ATT_PM_R_ENCRYPTION_REQUIRED       0x0010
#define ATT_PM_R_AUTHENT_MITM_REQUERED     0x0020
#define ATT_PM_W_AUTHENT_REQUIRED          0x0040
#define ATT_PM_W_AUTHORIZE_REQUIRED        0x0080
#define ATT_PM_W_ENCRYPTION_REQUIRED       0x0100
#define ATT_PM_W_AUTHENT_MITM_REQUERED     0x0200
#define ATT_PM_BR_ACCESS_ONLY              0x0400

//GATT Characteristic Descriptors
#define ATT_UUID_CHAR_EXT           0x2900
#define ATT_UUID_CHAR_USER      0x2901
#define ATT_UUID_CLIENT         0x2902
#define ATT_UUID_SERVER         0x2903
#define ATT_UUID_CHAR_FORMAT        0x2904
#define ATT_UUID_CHAR_AGGREGATE 0x2905

HANDLE bleslave_test_handle = NULL;

typedef struct ble_report_info
{
    unsigned char  eventid;
    char    state;
    UINT8   bleRcvBuffer[BLE_MAX_DATA_COUNT];
    UINT8    len;
    UINT16 uuid;
    UINT16 handle;
    UINT8 long_uuid[BLE_LONG_UUID_FLAG];
    UINT8 uuid_flag;
} ble_report_info_t;

typedef struct ble_add_characteristic
{
    T_OPENAT_BLE_CHARACTERISTIC_PARAM uuid_c; //特征uuid  
    T_OPENAT_BLE_DESCRIPTOR_PARAM *uuid_d; //特征描述
    UINT8 count;//描述数量
} ble_add_characteristic_t;



UINT16 connect_handle = 0xff;//连接句柄

static void AppConvertBinToHex(
    UINT8 *bin_ptr, // in: the binary format string
    UINT16 length,  // in: the length of binary string
    UINT8 *hex_ptr  // out: pointer to the hexadecimal format string
)
{
    UINT8 semi_octet = 0;
    int32 i = 0;

    for (i = 0; i < length; i++)
    {
        // get the high 4 bits
        semi_octet = (UINT8)((bin_ptr[i] & 0xF0) >> 4);
        if (semi_octet <= 9) //semi_octet >= 0
        {
            *hex_ptr = (UINT8)(semi_octet + '0');
        }
        else
        {
            *hex_ptr = (UINT8)(semi_octet + 'A' - 10);
        }

        hex_ptr++;

        // get the low 4 bits
        semi_octet = (UINT8)(bin_ptr[i] & 0x0f);
        if (semi_octet <= 9) // semi_octet >= 0
        {
            *hex_ptr = (UINT8)(semi_octet + '0');
        }
        else
        {
            *hex_ptr = (UINT8)(semi_octet + 'A' - 10);
        }
        hex_ptr++;
    }
}

void bluetooth_callback1(T_OPENAT_BLE_EVENT_PARAM *result)
{
    ble_report_info_t *msg = NULL;
    msg = iot_os_malloc(sizeof(ble_report_info_t));
	msg->eventid = result->id;
	msg->state  =  result->state;
	switch(result->id)
	{
		case OPENAT_BLE_RECV_DATA:
			if((result->len != 0) && (result->dataPtr != NULL))
			{
                msg->len  =  result->len;
                msg->uuid_flag  =  result->uuid_flag;
                msg->uuid  =  result->uuid;
                memcpy(msg->long_uuid, result->long_uuid, sizeof(result->long_uuid));
                memset(msg->bleRcvBuffer,0,sizeof(msg->bleRcvBuffer));
				memcpy(msg->bleRcvBuffer, result->dataPtr,result->len);  
                iot_os_send_message(bleslave_test_handle,msg);
			}
			break;
        case OPENAT_BLE_CONNECT_IND:
            msg->handle  =  result->handle;
            iot_os_send_message(bleslave_test_handle,msg);
            break;
        case OPENAT_BLE_DISCONNECT_IND:
            iot_debug_print("[bluetooth]bt1 disconnect");
			iot_bt_close();
			msg = iot_os_malloc(sizeof(ble_report_info_t));
			msg->eventid = OPENAT_BLE_RECV_DATA;
			msg->len = 0;
			iot_os_send_message(bleslave_test_handle,msg);
            break;
        case OPENAT_BT_ME_ON_CNF:
            iot_os_send_message(bleslave_test_handle,msg);
		    break;
		default:
            iot_os_free(msg);
		    break;
	}
}

BOOL ble_poweron1(VOID)
{
    iot_debug_print("[bluetooth]bt1 poweron");
    IVTBL(SetBLECallback)(bluetooth_callback1);
    IVTBL(OpenBT)(BLE_SLAVE);//打开蓝牙
    return TRUE;
}
/*先添加服务，在依次添加特征，若特征下包含特性描述，则添加特征后接着添加描述*/
BOOL AddService1()
{
    int i,j;
    U_OPENAT_BT_IOTCTL_PARAM  param1 = {0};
    U_OPENAT_BT_IOTCTL_PARAM  param2 = {0};
    U_OPENAT_BT_IOTCTL_PARAM  param3 = {0};
    T_OPENAT_BLE_UUID uuid = {0};
    T_OPENAT_BLE_CHARACTERISTIC_PARAM uuid_c1 = {0};
    T_OPENAT_BLE_CHARACTERISTIC_PARAM uuid_c2 = {0};
    T_OPENAT_BLE_DESCRIPTOR_PARAM uuid_d1 = {0};
    T_OPENAT_BLE_DESCRIPTOR_PARAM uuid_d2 = {0};
    uuid.uuid_short = BT_8910_TP_UUID;//服务uuid
    uuid.uuid_type = UUID_SHORT;
    uuid_c1.uuid.uuid_short = BT_8910_TP_UUID_CHAR;//特征uuid
    uuid_c1.uuid.uuid_type = UUID_SHORT;
    uuid_c1.attvalue = ATT_CHARA_PROP_READ | ATT_CHARA_PROP_WRITE;//特征属性
    uuid_c1.permisssion = ATT_PM_READABLE|ATT_PM_WRITEABLE;//特征权限
    uuid_c2.uuid.uuid_short = BT_8910_FEEDBACK_CHAR;//特征uuid
    uuid_c2.uuid.uuid_type = UUID_SHORT;
    uuid_c2.attvalue = ATT_CHARA_PROP_READ | ATT_CHARA_PROP_NOTIFY | ATT_CHARA_PROP_INDICATE;//特征属性
    uuid_c2.permisssion = ATT_PM_READABLE;//特征权限
    
    T_OPENAT_BLE_DESCRIPTOR_PARAM bt_descriptor[2] = {0};//特征描述
    bt_descriptor[0].uuid.uuid_short = ATT_UUID_CHAR_USER;//描述uuid
    bt_descriptor[0].uuid.uuid_type = UUID_SHORT;
    memcpy(bt_descriptor[0].value,"123456789",strlen("123456789"));//描述属性
    bt_descriptor[1].uuid.uuid_short = ATT_UUID_CLIENT;//描述uuid
    bt_descriptor[1].uuid.uuid_type = UUID_SHORT;
    bt_descriptor[1].configurationBits = 1;//描述属性
    ble_add_characteristic_t service_param[2] = {{uuid_c1,NULL,0},
                                        {uuid_c2,bt_descriptor,2}}; 

    param1.uuid = iot_os_malloc(sizeof(T_OPENAT_BLE_UUID));
    memcpy(param1.uuid,&uuid,sizeof(T_OPENAT_BLE_UUID));
    iot_ble_iotctl(0,BLE_ADD_SERVICE,param1);//添加服务
    iot_os_free(param1.uuid);
    param1.data = NULL;
    for(i = 0;i < sizeof(service_param)/sizeof(ble_add_characteristic_t);i ++)
    {
        param2.characteristicparam = iot_os_malloc(sizeof(T_OPENAT_BLE_CHARACTERISTIC_PARAM));
        memcpy(param2.characteristicparam,&service_param[i].uuid_c,sizeof(T_OPENAT_BLE_CHARACTERISTIC_PARAM));
        iot_ble_iotctl(0,BLE_ADD_CHARACTERISTIC,param2);//添加特征
        iot_os_free(param2.characteristicparam);
        param2.characteristicparam = NULL;
        if(service_param[i].count != 0)
        {
            for(j = 0;j < service_param[i].count;j ++)
            {
                param3.descriptorparam = iot_os_malloc(sizeof(T_OPENAT_BLE_DESCRIPTOR_PARAM));
                memcpy(param3.descriptorparam,&bt_descriptor[j],sizeof(T_OPENAT_BLE_DESCRIPTOR_PARAM));
                iot_ble_iotctl(0,BLE_ADD_DESCRIPTOR,param3);//添加描述
                iot_os_free(param3.descriptorparam);
                param3.descriptorparam = NULL;
            }
        }
    }

    return TRUE;
}

BOOL advertising1(VOID)
{
	iot_debug_print("[bluetooth]bt1 advertising");
    //U_OPENAT_BT_IOTCTL_PARAM  param1;
    U_OPENAT_BT_IOTCTL_PARAM  param2;
    U_OPENAT_BT_IOTCTL_PARAM  param3;
    U_OPENAT_BT_IOTCTL_PARAM  param5;
    T_OPENAT_BLE_ADV_DATA advdata;
    T_OPENAT_BLE_ADV_DATA scanrspdata;
    UINT8 data1[BLE_MAX_ADV_MUBER] = {0x02,0x01,0x06,0x04,0xff,0x01,0x02,0x03,0x0E,0x09,0x41,0x69,0x72,0x37,0x32,0x34,0x55,0x47,0x5F,0x43,0x53,0x44,0x4B};//广播包数据
    UINT8 data2[BLE_MAX_ADV_MUBER] = {0x02,0x0a,0x04};//响应包数据
/*
    param1.data = iot_os_malloc(strlen("Air724UG_CSDK"));
    memcpy(param1.data,"Air724UG_CSDK",strlen("Air724UG_CSDK"));
    iot_ble_iotctl(0,BLE_SET_NAME,param1);//设置广播名称
    iot_os_free(param1.data);
    param1.data = NULL;
*/
	memcpy(advdata.data,data1,BLE_MAX_ADV_MUBER);
    advdata.len = strlen(data1);
    param2.advdata = iot_os_malloc(sizeof(T_OPENAT_BLE_ADV_DATA));
    memcpy(param2.advdata,&advdata,sizeof(T_OPENAT_BLE_ADV_DATA));
    iot_ble_iotctl(0,BLE_SET_ADV_DATA,param2);//设置广播包数据
    iot_os_free(param2.advdata);
    param2.advdata = NULL;

	memcpy(scanrspdata.data,data2,BLE_MAX_ADV_MUBER);
    scanrspdata.len = strlen(data2);
    param3.advdata = iot_os_malloc(sizeof(T_OPENAT_BLE_ADV_DATA));
    memcpy(param3.advdata,&scanrspdata,sizeof(T_OPENAT_BLE_ADV_DATA));
    iot_ble_iotctl(0,BLE_SET_SCANRSP_DATA,param3);//设置响应包数据
    iot_os_free(param3.advdata);
    param3.advdata = NULL;

    AddService1();//添加自定义蓝牙服务

	iot_os_sleep(2000);
    param5.advEnable = 1;
    iot_ble_iotctl(0,BLE_SET_ADV_ENABLE,param5);//打开广播  
	iot_pmd_enter_deepsleep();
    return TRUE;
}

BOOL ble_data_trans1(VOID)
{
	int msgId;
    ble_report_info_t *msg = NULL;
    T_OPENAT_BLE_UUID uuid = {0};
    uuid.uuid_short = BT_8910_FEEDBACK_CHAR;
    uuid.uuid_type = UUID_SHORT;
    char *bleRcvBuffer = NULL;
    /*链接成功*/
    while(1)
    {
		iot_os_wait_message(bleslave_test_handle,&msg);//等待接收到数据
        if(msg->eventid == OPENAT_BLE_RECV_DATA)
        {
			if(msg->len == 0) 
            {
				if(msg != NULL)
                	iot_os_free(msg);
                msg = NULL;
                break;
            }
            bleRcvBuffer = iot_os_malloc(BLE_MAX_DATA_COUNT*2+1);                                                 
            iot_debug_print("[bluetooth]bt1 recv uuid %x",msg->uuid);
            iot_debug_print("[bluetooth]bt1 recv data len %d",msg->len);

            AppConvertBinToHex(msg->bleRcvBuffer,msg->len,bleRcvBuffer);
            bleRcvBuffer[msg->len*2] = '\0';
            iot_debug_print("[bluetooth]bt1 recv data %s",bleRcvBuffer);

            iot_ble_write(connect_handle,uuid,msg->bleRcvBuffer,msg->len);  

            iot_os_free(bleRcvBuffer);
            bleRcvBuffer = NULL;
			if(msg != NULL)
            	iot_os_free(msg);
            msg = NULL;
        }
    }
    return TRUE;
}

VOID slave_test1(VOID)
{
	int msgId;
    ble_report_info_t *msg = NULL;
    //1.  打开蓝牙
    ble_poweron1();
	OPENAT_wait_message(bleslave_test_handle, &msgId, &msg, 5000);//等待蓝牙打开
	if(msg == NULL) 
    {   
        DRV_PRINT_RESULT(FALSE);
        return FALSE;
    }
    if(msg->eventid == OPENAT_BT_ME_ON_CNF)
    {
		iot_debug_print("[bluetooth]bt1 open");
		if(msg != NULL)
        	iot_os_free(msg);
        msg = NULL;
        //2.广播蓝牙
        advertising1();
		OPENAT_wait_message(bleslave_test_handle, &msgId, &msg, 300*1000);//等待连接成功
		if(msg == NULL) 
		{   
			DRV_PRINT_RESULT(FALSE);
			iot_bt_close();
			return FALSE;
		}
        if(msg->eventid == OPENAT_BLE_CONNECT_IND)
        {     
            //3. 数据传输
            connect_handle = msg->handle;//连接句柄
			iot_debug_print("[bluetooth]bt1 connect %d",connect_handle);
			if(msg != NULL)
            	iot_os_free(msg);
            msg = NULL;
			iot_pmd_exit_deepsleep();
            ble_data_trans1();
        }
		else
        {
			if(msg != NULL)
            	iot_os_free(msg);
            msg = NULL;
			DRV_PRINT_RESULT(FALSE);
        }
    }
	else
	{
		if(msg != NULL)
			iot_os_free(msg);
		msg = NULL;
		DRV_PRINT_RESULT(FALSE);
	}
}



static HANDLE hTimer;

typedef struct BT_ADDRESS1
{
    uint8 addr[6];
} BT_ADDRESS_t1;

typedef struct _ble_scan_report_info1
{
    UINT8 name_length;
    UINT8 name[BLE_MAX_ADV_MUBER+1];
    UINT8 addr_type;
    BT_ADDRESS_t1 bdAddress;
    UINT8 event_type;
    UINT8 data_length;
    UINT8 manu_data[BLE_MAX_ADV_MUBER+1];
    UINT8 manu_len;
    UINT8 raw_data[BLE_MAX_ADV_MUBER+1];
    UINT8 rssi;
} ble_scan_report_info1;

typedef struct ble_report_info1
{
    unsigned char  eventid;
    char    state;
    UINT8   bleRcvBuffer[BLE_MAX_DATA_COUNT];
    UINT8    len;
    UINT8 enable;
    UINT16 uuid;
    UINT16 handle;
    UINT8 long_uuid[BLE_LONG_UUID_FLAG];
    UINT8 uuid_flag;
} ble_report_info_t1;

static void ble_stop_timer()
{
    if(hTimer != NULL)
    {
        OPENAT_stop_timer(hTimer);
        OPENAT_delete_timer(hTimer);
        hTimer = NULL;
    }
}

BOOL ble_close2(VOID)
{
    ble_report_info_t1 *msg = NULL;
    iot_ble_disconnect(connect_handle);
    ble_stop_timer();
    iot_bt_close();
    msg = iot_os_malloc(sizeof(ble_report_info_t1));
    msg->eventid = OPENAT_BLE_RECV_DATA;
    msg->len = 0;
    iot_os_send_message(bleslave_test_handle,msg);
}

static void ble_start_timer(UINT32 interval_ms)
{
    if(hTimer == NULL)
    {
        hTimer = OPENAT_create_timer(ble_close2, NULL);
        if(hTimer != NULL)
            OPENAT_start_timer(hTimer, interval_ms);
    }
}

void bluetooth_callback2(T_OPENAT_BLE_EVENT_PARAM *result)
{
    char scanaddr[20] = {0};
    ble_scan_report_info1 *ble_scanInfo = NULL;
    ble_report_info_t1 *msg = NULL;
    U_OPENAT_BT_IOTCTL_PARAM  param = {0};
    msg = iot_os_malloc(sizeof(ble_report_info_t1));
	msg->eventid = result->id;
	msg->state  =  result->state;
    char manu_data[BLE_MAX_ADV_MUBER*2+1] = {0};
    char raw_data[BLE_MAX_ADV_MUBER*2+1] = {0};
    char long_uuid[BLE_LONG_UUID_FLAG*2] = {0};
	switch(result->id)
	{
		case OPENAT_BLE_RECV_DATA:
			if((result->len != 0) && (result->dataPtr != NULL))
			{
                msg->len  =  result->len;
                msg->uuid_flag  =  result->uuid_flag;//1:128位uuid 0:16位uuid
                msg->uuid  =  result->uuid;//16位uuid
                memcpy(msg->long_uuid, result->long_uuid, sizeof(result->long_uuid));//128位uuid
                memset(msg->bleRcvBuffer,0,sizeof(msg->bleRcvBuffer));
				memcpy(msg->bleRcvBuffer, result->dataPtr,result->len);  
                iot_os_send_message(bleslave_test_handle,msg);
			}
			break;
        case OPENAT_BLE_CONNECT:
            msg->handle  =  result->handle;
            iot_os_send_message(bleslave_test_handle,msg);
            break;
        case OPENAT_BLE_DISCONNECT:
            iot_debug_print("[bluetooth]bt2 disconnect");
            iot_os_free(msg);
            break;
        case OPENAT_BT_ME_ON_CNF:
        case OPENAT_BLE_SET_SCAN_ENABLE:
        case OPENAT_BLE_FIND_CHARACTERISTIC_IND:
            iot_os_send_message(bleslave_test_handle,msg);
		    break;
        case OPENAT_BLE_SET_SCAN_REPORT:
            ble_scanInfo = (ble_scan_report_info1 *)result->dataPtr;
            iot_debug_print("[bluetooth]bt2 scan name %s",ble_scanInfo->name);//蓝牙名称
            iot_debug_print("[bluetooth]bt2 scan rssi %d",(int8)ble_scanInfo->rssi);//信号强度
            sprintf(scanaddr, "%02x:%02x:%02x:%02x:%02x:%02x", ble_scanInfo->bdAddress.addr[0], ble_scanInfo->bdAddress.addr[1], ble_scanInfo->bdAddress.addr[2], ble_scanInfo->bdAddress.addr[3], ble_scanInfo->bdAddress.addr[4], ble_scanInfo->bdAddress.addr[5]);
            iot_debug_print("[bluetooth]bt2 scan addr %s",scanaddr);//蓝牙地址
            iot_debug_print("[bluetooth]bt2 scan addr_type %d",ble_scanInfo->addr_type);//地址种类

            AppConvertBinToHex(ble_scanInfo->manu_data,ble_scanInfo->manu_len,manu_data);
            manu_data[ble_scanInfo->manu_len*2] = '\0';
            iot_debug_print("[bluetooth]bt2 scan manu_data %s",manu_data);//厂商数据
            AppConvertBinToHex(ble_scanInfo->raw_data,ble_scanInfo->data_length,raw_data);
            raw_data[ble_scanInfo->data_length*2] = '\0';
            iot_debug_print("[bluetooth]bt2 scan raw_data %s",raw_data);//广播原始数据
            
            if(memcmp(ble_scanInfo->name,"Air724UG_CSDK",strlen("Air724UG_CSDK")) == 0) //连接的蓝牙名称
            {
                param.advEnable = 0;
                iot_ble_iotctl(0,BLE_SET_SCAN_ENABLE,param);//关闭扫描
                iot_ble_connect(scanaddr,ble_scanInfo->addr_type);//连接蓝牙
            }
            iot_os_free(msg);
		    break;
        case OPENAT_BLE_FIND_SERVICE_IND:
            iot_debug_print("[bluetooth]bt2 service uuid %x",result->uuid);
            if(result->uuid == 0xfee0)
                iot_os_send_message(bleslave_test_handle,msg);
            else
                iot_os_free(msg);
            break;
        case OPENAT_BLE_FIND_CHARACTERISTIC_UUID_IND:
            if(result->uuid_flag == UUID_SHORT)//1:128位uuid 0:16位uuid
            {
                iot_debug_print("[bluetooth]bt2 characteriatic uuid %x",result->uuid);
            }
            else if(result->uuid_flag == UUID_LONG)
            {
                AppConvertBinToHex(result->long_uuid,sizeof(result->long_uuid),long_uuid);
                iot_debug_print("[bluetooth]bt2 characteriatic uuid %s",long_uuid);
            }
            iot_os_free(msg);
            break;
		default:
		    break;
	}
}

BOOL ble_poweron2(VOID)
{
    iot_debug_print("[bluetooth]bt2 poweron");

    IVTBL(SetBLECallback)(bluetooth_callback2);
    IVTBL(OpenBT)(BLE_MASTER);//打开蓝牙
    return TRUE;
}

BOOL scan2(VOID)
{
    int msgId;
    int i;
    U_OPENAT_BT_IOTCTL_PARAM  param2 = {0};
    ble_report_info_t1 *msg = NULL;
    UINT8 connect_addr_type = 0;
    char connect_scanaddr[20] = {0};
    iot_debug_print("[bluetooth]bt2 scan");
    param2.advEnable = 1;
    iot_ble_iotctl(0,BLE_SET_SCAN_ENABLE,param2);//打开扫描
    OPENAT_wait_message(bleslave_test_handle, &msgId, &msg, 10000);
	if(msg == NULL) 
    {   
        DRV_PRINT_RESULT(FALSE);
		iot_bt_close();
        return FALSE;
    }
    if(msg->eventid != OPENAT_BLE_SET_SCAN_ENABLE)//等待扫描使能成功
    {
		if(msg != NULL)
        	iot_os_free(msg);
        msg = NULL;
		DRV_PRINT_RESULT(FALSE);
		iot_bt_close();
        return FALSE;
    }
	if(msg != NULL)
    	iot_os_free(msg);
    msg = NULL;
    return TRUE;
}
static UINT8 slave_connect_interrupt = 0;

BOOL ble_data_trans2(VOID)
{
    int msgId;
    T_OPENAT_BLE_UUID uuid = {0};
    T_OPENAT_BLE_UUID uuid_s = {0};
    T_OPENAT_BLE_UUID uuid_c = {0};
    ble_report_info_t1 *msg = NULL;
    char *bleRcvBuffer = NULL;
    U_OPENAT_BT_IOTCTL_PARAM  param1 = {0};
    U_OPENAT_BT_IOTCTL_PARAM  param2 = {0};
    U_OPENAT_BT_IOTCTL_PARAM  param3 = {0};
    
    uuid.uuid_short = 0xfee1;
    uuid.uuid_type = UUID_SHORT;
    uuid_s.uuid_short = 0xfee0;
    uuid_s.uuid_type = UUID_SHORT;
    uuid_c.uuid_short = 0xfee2;
    uuid_c.uuid_type = UUID_SHORT;
    //UINT8 uuid_s[16] = {0xF2, 0xC3, 0xF0, 0xAE, 0xA9, 0xFA, 0x15, 0x8C, 0x9D, 0x49, 0xAE, 0x73, 0x71, 0x0A, 0x81, 0xE7};
    //UINT8 uuid_c[16] = {0x9F, 0x9F, 0x00, 0xC1, 0x58, 0xBD, 0x32, 0xB6, 0x9E, 0x4C, 0x21, 0x9C, 0xC9, 0xD6, 0xF8, 0xBE};
    iot_debug_print("[bluetooth]bt2 data trans  %d",connect_handle);
    
    /*连接成功，发现包含的服务及特征，并打开通知*/
    iot_ble_iotctl(connect_handle,BLE_FIND_SERVICE,param1);//发现服务
    OPENAT_wait_message(bleslave_test_handle, &msgId, &msg, 10000);
	if(msg == NULL) 
	{
		iot_debug_print("[bluetooth]bt2 connect interrupt");
		slave_connect_interrupt = 1;
		return FALSE;
	}
    if(msg->eventid != OPENAT_BLE_FIND_SERVICE_IND)//等待发现服务成功
    {
		if(msg != NULL)
    		iot_os_free(msg);
    	msg = NULL;
        return FALSE;
    }
	if(msg != NULL)
    	iot_os_free(msg);
    msg = NULL;
    
    param2.uuid = iot_os_malloc(sizeof(T_OPENAT_BLE_UUID));
    memcpy(param2.uuid,&uuid_s,sizeof(T_OPENAT_BLE_UUID));
    iot_ble_iotctl(connect_handle,BLE_FIND_CHARACTERISTIC,param2);//发现服务内的特征
    iot_os_free(param2.uuid);
    param2.uuid = NULL;
    OPENAT_wait_message(bleslave_test_handle, &msgId, &msg, 10000);
	if(msg == NULL) 
    {   
        DRV_PRINT_RESULT(FALSE);
		iot_bt_close();
        return FALSE;
    }
    if(msg->eventid != OPENAT_BLE_FIND_CHARACTERISTIC_IND)//等待发现特征成功
    {
		if(msg != NULL)
    		iot_os_free(msg);
    	msg = NULL;
		DRV_PRINT_RESULT(FALSE);
		iot_bt_close();
        return FALSE;
    }
	if(msg != NULL)
    	iot_os_free(msg);
    msg = NULL;
    param3.uuid = iot_os_malloc(sizeof(T_OPENAT_BLE_UUID));
    memcpy(param3.uuid,&uuid_c,sizeof(T_OPENAT_BLE_UUID));
    iot_ble_iotctl(connect_handle,BLE_OPEN_NOTIFICATION,param3);//打开通知
    iot_os_free(param3.uuid);
    param3.uuid = NULL;
    ble_start_timer(1800*1000);
    while(1)
    {
        iot_ble_write(connect_handle,uuid,"1234567890",strlen("1234567890"));
        iot_os_wait_message(bleslave_test_handle,&msg);//等待接收到数据
        if(msg->eventid == OPENAT_BLE_RECV_DATA)
        {
            if(msg->len == 0) 
            {
				if(msg != NULL)
                	iot_os_free(msg);
                msg = NULL;
                break;
            }  
            bleRcvBuffer = iot_os_malloc(BLE_MAX_DATA_COUNT*2+1);
            iot_debug_print("[bluetooth]bt2 recv uuid %x",msg->uuid);
            iot_debug_print("[bluetooth]bt2 recv data len %d",msg->len);  

            AppConvertBinToHex(msg->bleRcvBuffer,msg->len,bleRcvBuffer);
            bleRcvBuffer[msg->len*2] = '\0';
            iot_debug_print("[bluetooth]bt2 recv data %s",bleRcvBuffer);

            iot_os_free(bleRcvBuffer);
            bleRcvBuffer = NULL;
			if(msg != NULL)
            	iot_os_free(msg);
            msg = NULL;
        }
    }
    return TRUE;
}

VOID slave_test2(VOID)
{
	int msgId;
    ble_report_info_t1 *msg = NULL;
    //1.  打开蓝牙
	while(1)
	{
		ble_poweron2();
		OPENAT_wait_message(bleslave_test_handle, &msgId, &msg, 5000);//等待蓝牙打开
		if(msg == NULL) 
		{   
			DRV_PRINT_RESULT(FALSE);
			return FALSE;
		}
		if(msg->eventid == OPENAT_BT_ME_ON_CNF)
		{
			iot_debug_print("[bluetooth]bt2 open");
			if(msg != NULL)
				iot_os_free(msg);
			msg = NULL;
			//2.扫描蓝牙
			
			scan2();
			OPENAT_wait_message(bleslave_test_handle, &msgId, &msg, 300*1000);//等待连接成功
			if(msg == NULL) 
			{   
				DRV_PRINT_RESULT(FALSE);
				iot_bt_close();
				return FALSE;
			}
			if(msg->eventid == OPENAT_BLE_CONNECT)
			{ 
				//3. 数据传输
				connect_handle = msg->handle;//连接句柄
				iot_debug_print("[bluetooth]bt2 connect %d",connect_handle);
				if(msg != NULL)
					iot_os_free(msg);
				msg = NULL;
				ble_data_trans2();
				if(slave_connect_interrupt == 0)
					break;
				else
				{
					iot_bt_close();
					iot_os_sleep(1000);
					slave_connect_interrupt = 0;
				}
			}
			else
			{
				if(msg != NULL)
					iot_os_free(msg);
				msg = NULL;
				DRV_PRINT_RESULT(FALSE);
			}
		}
		else
		{
			if(msg != NULL)
				iot_os_free(msg);
			msg = NULL;
			DRV_PRINT_RESULT(FALSE);
		}
	}
}

BOOL ble_poweron3(VOID)
{
    iot_debug_print("[bluetooth]bt3 poweron");
    IVTBL(SetBLECallback)(bluetooth_callback1);
    IVTBL(OpenBT)(BLE_SLAVE);//打开蓝牙
    return TRUE;
}

BOOL advertising3(VOID)
{
    U_OPENAT_BT_IOTCTL_PARAM  param1;
    U_OPENAT_BT_IOTCTL_PARAM  param3;
    UINT8 uuid[16] = {0xAB,0x81,0x90,0xD5,0xD1,0x1E,0x49,0x41,0xAC,0xC4,0x42,0xF3,0x05,0x10,0xB4,0x08};
    T_OPENAT_BLE_BEACON_DATA beacondata = {0};
    memcpy(beacondata.uuid,uuid,16);
    beacondata.major = 10107;
    beacondata.minor = 50179;
    iot_debug_print("[bluetooth]bt3 advertising");

    param1.beacondata = iot_os_malloc(sizeof(T_OPENAT_BLE_BEACON_DATA));
    memcpy(param1.beacondata,&beacondata,sizeof(T_OPENAT_BLE_BEACON_DATA));
    iot_ble_iotctl(0,BLE_SET_BEACON_DATA,param1);//设置beacon数据
    iot_os_free(param1.beacondata);
    param1.beacondata = NULL;

    iot_os_sleep(500);
    param3.advEnable = 1;
    iot_ble_iotctl(0,BLE_SET_ADV_ENABLE,param3);//打开广播  
    return TRUE;
}

VOID slave_test3(VOID)
{
	int msgId;
    ble_report_info_t *msg = NULL;
    //1.  打开蓝牙
    ble_poweron3();
	OPENAT_wait_message(bleslave_test_handle, &msgId, &msg, 5000);//等待蓝牙打开
	if(msg == NULL) 
    {   
        DRV_PRINT_RESULT(FALSE);
        return FALSE;
    }
    if(msg->eventid == OPENAT_BT_ME_ON_CNF)
    {
		iot_debug_print("[bluetooth]bt3 open");
		if(msg != NULL)
        	iot_os_free(msg);
        msg = NULL;
        //2.广播蓝牙
        advertising3();
		iot_os_sleep(10000);
		iot_bt_close();
    }
	else
	{
		if(msg != NULL)
			iot_os_free(msg);
		msg = NULL;
		DRV_PRINT_RESULT(FALSE);
	}
}

static void bleslave_test(VOID)
{
	while(1)
	{
		DRV_PRINT("[%s,%d]test slave start", __FUNCTION__, __LINE__);
		slave_test1();
		iot_os_sleep(10000);
		slave_test2();
		iot_os_sleep(10000);
		slave_test3();
		iot_os_sleep(10000);
	}
}

void pub_enable_slave(void)
{
    bleslave_test_handle = iot_os_create_task(bleslave_test, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "slave");
}
