#include "driver_comm.h"


#undef DRV_NAME
#define DRV_NAME "DRV_I2C"
#define I2C_SALVE_ADDR (0X3C)
#define I2C_LCD_WIDTH (128)
#define I2C_LCD_HEIGHT (64)
static HANDLE testTask;

CONST UINT8 cmdG[] = {0x00, 0x00, 0xf0, 0x07, 0xf8, 0x0f, 0x08, 0x08, 0x98, 0x0f, 0x90, 0x07, 0x00, 0x00};
CONST UINT8 cmdO[] = {0x00, 0x00, 0xf0, 0x07, 0xf8, 0x0f, 0x08, 0x08, 0xf8, 0x0f, 0xf0, 0x07, 0x00, 0x00};
CONST UINT8 initCmd[] = {0xAE, 0X00, 0x10, 0x40, 0x81, 0x7f, 0xA1, 0XA6, 0XA8, 63, 0XC8, 0XD3, 0X00, 0XD5, 0X80, 0XDA, 0X12, 0X8D, 0X14, 0X20, 0X01, 0XAF, 0X21, 0X00, 0X7F, 0X22, 0X00, 0X07};
CONST UINT8 cmdReg = 0x00;
CONST UINT8 dataReg = 0x40;

static UINT32 prvLcdSend(UINT8 data, BOOL isCmd)
{
	UINT8 cmd;
	
	if (isCmd)
	{
		cmd = 0;
	}
	else
	{
		cmd = 0x40;
	}

	return iot_i2c_write(OPENAT_I2C_2, I2C_SALVE_ADDR, &cmd, &data, 1);
}

static void prvI2cLcdShowGo(int columnStart,int columnEnd, int pageStart, int pageEnd)
{
	int i;
	
	prvErrOutput(prvLcdSend(0x21, TRUE));
	prvErrOutput(prvLcdSend(columnStart, TRUE));
	prvErrOutput(prvLcdSend(columnEnd, TRUE));

	prvErrOutput(prvLcdSend(0X22, TRUE));
	prvErrOutput(prvLcdSend(pageStart, TRUE));
	prvErrOutput(prvLcdSend(pageEnd, TRUE));

	
	for (i=1; i<sizeof(cmdG); i++)
		prvErrOutput(prvLcdSend(cmdG[i], FALSE));
    
    for (i=1; i<sizeof(cmdO); i++)
		prvErrOutput(prvLcdSend(cmdO[i], FALSE));

}

static void prvTest(void)
{
	T_AMOPENAT_I2C_PARAM param;
	int i,j;
	
	memset(&param, 0, sizeof(param));
	param.freq = 100000;
	param.regAddrBytes = I2C_SALVE_ADDR;
	prvErrOutput(iot_i2c_open(OPENAT_I2C_2, &param));

	/*设置初始化表*/
	for (i=0; i<sizeof(initCmd); i++)
	{
		prvErrOutput(prvLcdSend(initCmd[i], TRUE));
	}

	/*清除屏幕*/
	for (i=0; i<I2C_LCD_WIDTH*I2C_LCD_HEIGHT/8; i++)
	{
		prvErrOutput(prvLcdSend(0, FALSE));
	}
	
	iot_os_sleep(4000);

	for (j=0; j<7; j+=2)
	{
		for (i=0; i<I2C_LCD_WIDTH;i+=28)
		{
			prvI2cLcdShowGo(i,i+28,j,j+1);
			iot_os_sleep(100);
		}
	}

	iot_os_sleep(400);
	prvErrOutput(iot_i2c_close(OPENAT_I2C_2));
	
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		
		prvTest();

		iot_os_sleep(5000);
		
	}
}

void i2cTest(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "i2c");
}

