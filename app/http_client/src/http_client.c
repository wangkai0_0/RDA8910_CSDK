/*
 * @Author: your name
 * @Date: 2020-05-27 10:45:13
 * @LastEditTime: 2020-05-31 17:24:25
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\app\http_client\src\http_client.c
 */

#include <stdlib.h>
#include "string.h"
#include "cs_types.h"
#include "iot_debug.h"
#include "iot_socket.h"
#include "iot_network.h"

#include "http_client.h"
#include "http_init.h"
#include "http_net.h"

//修改host数据，必须先创建一个client对象才可以修改
void http_set_host(http_client_handle_t *client, char *host)
{
    if (client->host != NULL)
    {
        if (host != NULL)
        {
            memcpy(client->host, host, strlen(host));
        }
        else
        {
            iot_debug_print("[http_client] Host is empty!");
        }
    }
    else
    {
        iot_debug_print("[http_client] Please Create HTTP first!");
    }
}
//获取host数据
char *http_get_host(http_client_handle_t *client)
{
    return client->host;
}

//修改ip数据，必须先创建一个client对象才可以修改
void http_set_ip(http_client_handle_t *client, char *ip)
{
    if (client->ip != NULL)
    {
        if (ip != NULL)
        {
            memcpy(client->ip, ip, strlen(ip));
        }
        else
        {
            iot_debug_print("[http_client] ip is empty!");
        }
    }
    else
    {
        iot_debug_print("[http_client] Please Create HTTP first!");
    }
}
//获取ip数据
char *http_get_ip(http_client_handle_t *client)
{
    return client->ip;
}

//修改port数据，必须先创建一个client对象才可以修改
void http_set_port(http_client_handle_t *client, int port)
{
    if (client->port != NULL)
    {
        if (port != 0)
        {
            client->port = port;
        }
        else
        {
            iot_debug_print("[http_client] port is zero!");
        }
    }
    else
    {
        iot_debug_print("[http_client] Please Create HTTP first!");
    }
}
//获取port数据
int http_get_port(http_client_handle_t *client)
{
    return client->port;
}

//修改path数据，必须先创建一个client对象才可以修改
void http_set_path(http_client_handle_t *client, char *path)
{
    if (client->path != NULL)
    {
        if (path != NULL)
        {
            memcpy(client->path, path, strlen(path));
        }
        else
        {
            iot_debug_print("[http_client] Path is empty!");
        }
    }
    else
    {
        iot_debug_print("[http_client] Please Create HTTP first!");
    }
}
//获取path数据
char *http_get_path(http_client_handle_t *client)
{
    return client->path;
}

//修改table_data数据，必须先创建一个client对象才可以修改
void http_set_table_data(http_client_handle_t *client, char *table_data)
{
    if (client->table_data != NULL)
    {
        if (table_data != NULL)
        {
            memcpy(client->table_data, table_data, strlen(table_data));
        }
        else
        {
            iot_debug_print("[http_client] table_data is empty!");
        }
    }
    else
    {
        iot_debug_print("[http_client] Please Create HTTP first!");
    }
}
//获取table_data数据
char *http_get_table_data(http_client_handle_t *client)
{
    return client->table_data;
}

//修改method数据，必须先创建一个client对象才可以修改
void http_set_method(http_client_handle_t *client, http_client_method_t method)
{
    if (client->method != NULL)
    {
        client->method = method;
    }
    else
    {
        iot_debug_print("[http_client] Please Create HTTP first!");
    }
}
//获取method数据
http_client_method_t http_get_method(http_client_handle_t *client)
{
    return client->method;
}

//修改event_handler数据，必须先创建一个client对象才可以修改
void http_set_event_handler(http_client_handle_t *client, http_event_handle_cb event_handler)
{
    if (client->event_handler != NULL)
    {
        if (event_handler != NULL)
        {
            client->event_handler = event_handler;
        }
        else
        {
            iot_debug_print("[http_client] event_handler is empty!");
        }
    }
    else
    {
        iot_debug_print("[http_client] Please Create HTTP first!");
    }
}

//删除http对象
void http_delete(http_client_handle_t *client)
{
    /////////////////////http_data_buffer_init//////////////////////////////////
    if (client->buffer_rx != NULL)
    { //释放接收数据缓冲区
        iot_os_free(client->buffer_rx);
    }

    if (client->buffer_tx != NULL)
    { //释放发送数据缓冲区
        iot_os_free(client->buffer_tx);
    }

    if (client->buffer_respons_header != NULL)
    { ///释放响应头数据存储区
        iot_os_free(client->buffer_respons_header);
    }

    if (client->respons_state != NULL)
    { ///释放响应头状态存储区
        iot_os_free(client->respons_state);
    }
    client->Range_start = 0;
    client->Range_end = 0;
    //////////////////////////////////////////////////////////
    if (client->host != NULL)
    { ///释放响应头状态存储区
        iot_os_free(client->host);
    }
    if (client->ip != NULL)
    { ///释放响应头状态存储区
        iot_os_free(client->ip);
    }
    client->port = 0;
    if (client->path != NULL)
    { ///释放响应头状态存储区
        iot_os_free(client->path);
    }
    if (client->table_data != NULL)
    { ///释放响应头状态存储区
        iot_os_free(client->table_data);
    }
}

//获取初始化状态
http_client_init_state http_get_init_state(http_client_handle_t *client)
{
    return client->init_state;
}
//新建一个http的对象，每次调用都会新建一个独立的对象，不会修改原有的对象
http_client_handle_t http_client_new(http_client_config_t *config)
{
    http_client_handle_t client = {0};
    http_data_buffer_init(config, &client);

    client.method = config->method;
    //请求的url
    if (config->url != NULL)
    {
        http_parse_request_url(config, &client);
    }
    else
    {
        if (http_host_init(config, &client) == FALSE)
            return client;
        if (http_port_init(config, &client) == FALSE)
            return client;

        http_path_init(config, &client);
        http_table_init(config, &client);
    }
    if (client.method == HTTP_METHOD_POST)
    {
        http_table_init(config, &client);
    }

    if (http_event_handler_init(config, &client) == FALSE)
        return client;
    if (http_ip_init(&client) == FALSE)
        return client;
    client.init_state = HTTP_INIT_OK;
    return client;
}
//执行发送请求
void http_client_perform(http_client_handle_t *client)
{
    client->perform_task_handle = iot_os_create_task(http_client_perform_task, (PVOID)client, 2048, 10, OPENAT_OS_CREATE_DEFAULT, "http_client_perform_task");
}