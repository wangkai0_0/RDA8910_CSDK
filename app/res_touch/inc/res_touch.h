#ifndef __RES_TOUCH_H
#define __RES_TOUCH_H

#include "am_openat_drv.h"
#include "iot_spi.h"
#include "iot_gpio.h"
#include "iot_debug.h"

// /* touch panel interface define */
#define CMD_RDX 0xD0 //����IC�����������
#define CMD_RDY 0x90

//#define PEN  		iot_gpio_set()//PBin(1)  	//T_PEN
#define TDIN                        \
	({                              \
		UINT8 Penirq = 0;           \
		iot_gpio_read(12, &Penirq); \
		Penirq;                     \
	})

#define DOUT(value) iot_gpio_set(11, value) //PFout(11)						//T_MOSI
#define TCLK(value) iot_gpio_set(9, value)	//PBout(0)  	//T_SCK
#define TCS(value) iot_gpio_set(10, value)	//PCout(5)  	//T_CS

BOOL TP_Read_Adjdata(void);
BOOL TP_Read_Screen_XY(u16 *x, u16 *y);
BOOL Read_ADS(u16 *x, u16 *y);
BOOL Read_ADS2(u16 *x, u16 *y, u8 errRange);
BOOL Touch_Adjust(void);
void Touch_init(void);

#endif
