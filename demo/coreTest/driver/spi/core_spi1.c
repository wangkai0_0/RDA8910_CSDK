#include "driver_comm.h"
#include "norflash.h"

#undef DRV_NAME
#define DRV_NAME "DRV_SPI1"

static HANDLE testTask;

#define SPI1_TEST_LEN (0X1000)
static char gSendBuff[SPI1_TEST_LEN];
static char gReadBuff[SPI1_TEST_LEN];

static void prvTest(void)
{
	T_AMOPENAT_SPI_PARAM cfg = {
		.clock = 1000000,
		.fullDuplex = TRUE,
		.cpha = 0,
		.cpol = 0,
		.dataBits = 8,
		.withCS = FALSE
	};
		
	iot_spi_close(OPENAT_SPI_1);

	DRV_PRINT_RESULT(iot_spi_open(OPENAT_SPI_1, &cfg));

	memset(gSendBuff, 0x34, sizeof(gSendBuff));
	
	DRV_PRINT_RESULT(iot_spi_rw(OPENAT_SPI_1, (CONST UINT8*)gSendBuff, (UINT8*)gReadBuff, sizeof(gSendBuff)));

	if(memcmp(gSendBuff, gReadBuff, sizeof(gSendBuff)) != 0)
	{
		DRV_PRINT_RESULT(FALSE);
	}
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		prvTest();
		
		iot_os_sleep(5000);
	}
}

void pub_enable_spi1(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "spi");
}
