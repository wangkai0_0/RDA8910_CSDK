// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// File name    : norflash.h
// Version      : V0.1
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#ifndef NORFLASH_H_
#define NORFLASH_H_
#include "iot_spi.h"
#include "iot_debug.h"

typedef enum
{
	WriteEnable			= 0x06,
	WriteDisable		= 0x04,
	ReadStatusRegister	= 0x05,
	WriteStatusRegister = 0x01,
	ReadData			= 0x03,
	FastRead_Data		= 0x0B,
	PageProgram			= 0x02,
	SectorErase			= 0x20,
	BlockErase			= 0xD8,
	ChipErase			= 0xC7,
	PowerDown			= 0xB9,
	Release_PowerDown	= 0xAB,
	Read_Manufacture_ID = 0x90,
	Read_JEDEC_ID		= 0x9F

}NORFLASH_CMD;;
#define NOR_SECTOR_SIZE		0x1000
#define NOR_BLOCK_SIZE		0x10000
#define NOR_PAGE_SIZE       0x100

// #define NOR_FLASH_SPI      SPI0

#define NOR_FLASH_CS_H     iot_gpio_set(10, 1)
#define NOR_FLASH_CS_L     iot_gpio_set(10, 0)


extern bool NorFlash_Init(void);
void ReadNorID(u8 *read_buf);
extern void NorReadData(u32 srcAddr,u8 *dest,u32 u4Len);
extern void NorWriteData(u8 *src,u32 destAddr,u32 u4Len);
extern void NorSectorErase(u32 u4Addr, u32 u4Len);
extern void NorFlash_Demo(void);
void NorAllChipErase(void);


#endif /* NORFLASH_H_ */
