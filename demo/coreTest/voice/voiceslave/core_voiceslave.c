#include "string.h"
#include "iot_debug.h"
#include "driver_comm.h"

#define VOICE_TASK_PRIO (24)
#define VOICE_TASK_STACK_SIZE (4096)

static void voiceslave_test(VOID)
{
	while(1)
	{

	}
}

void pub_enable_voiceslave(void)
{
    iot_os_create_task(voiceslave_test, NULL, VOICE_TASK_STACK_SIZE, VOICE_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "voiceslave");
}
