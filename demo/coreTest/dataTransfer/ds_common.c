#include "ds_common.h"

E_OPENAT_NETWORK_STATE g_network_state = OPENAT_NETWORK_DISCONNECT;

static VOID network_ind_cb(E_OPENAT_NETWORK_STATE state)
{
	g_network_state = state;
}


void network_ind_init()
{
	iot_network_set_cb(network_ind_cb);	
}



