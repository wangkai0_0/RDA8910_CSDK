#include "driver_comm.h"
#include "norflash.h"

#undef DRV_NAME
#define DRV_NAME "DRV_SPI"

static HANDLE testTask;

static char gSendBuff[NOR_PAGE_SIZE];
static char gReadBuff[NOR_PAGE_SIZE];

static void prvTest(void)
{
	UINT32 flashId = 0;
	UINT32 addr = 0;
	int i;
	prvErrOutput(NorFlash_Init());
	iot_os_sleep(100);
	ReadNorID((u8 *)&flashId);
	iot_os_sleep(100);
	DRV_PRINT("[%s,%d] flashId %x", __FUNCTION__, __LINE__, flashId);

	NorAllChipErase();
	iot_os_sleep(100);
	memset(gSendBuff, 0x5c, NOR_PAGE_SIZE);

	for (i=0; i<0x4000; i++)
	{
		NorWriteData((u8 *)gSendBuff, addr, NOR_PAGE_SIZE);
		memset(gReadBuff, 0, NOR_PAGE_SIZE);
		NorReadData(addr, (u8 *)gReadBuff, NOR_PAGE_SIZE);

		if (memcmp(gReadBuff, gSendBuff, NOR_PAGE_SIZE) == 0)
		{
			if (addr % NOR_BLOCK_SIZE == 0)
			{
				DRV_PRINT("[%s,%d] addr %x", __FUNCTION__, __LINE__, addr);
			}
		}
		else
		{
			int j,count = 0;
			for (j=0; j<NOR_PAGE_SIZE; j++)
			{
				if ((gReadBuff[j] != 0x5c) && (count < 30))
				{
					count++;
					DRV_PRINT_ERR("[%s,%d] addr %x buff[%d]=%x, count %d", __FUNCTION__, __LINE__, addr, j, gReadBuff[j], count);
				}
			}
		}
		addr += NOR_PAGE_SIZE;

		iot_os_sleep(3);
	}
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		prvTest();
		
		iot_os_sleep(5000);
	}
}

void spiTest(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "spi");
}
