// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// File name    : norflash.c
// Version      : V0.1
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include "stdio.h"
#include "norflash.h"
#include "driver_comm.h"


#define NORFLASH_BUSY		(1<<0)
#define	NORFLASH_WEL		(1<<1)/*write enable latch*/
#define NOR_FLASH_SPI   OPENAT_SPI_1

#define TRACE_NOTICE 

bool NorFlash_Init(void)
{
    BOOL res = FALSE;
     
    T_AMOPENAT_SPI_PARAM cfg = {
        .clock = 1000000,
        .fullDuplex = TRUE,
        .cpha = 0,
        .cpol = 0,
        .dataBits = 8,
        .withCS = FALSE
    };
	iot_spi_close(NOR_FLASH_SPI);
    res = iot_spi_open(NOR_FLASH_SPI, &cfg);

    T_AMOPENAT_GPIO_CFG  output_cfg;
    memset(&output_cfg, 0, sizeof(T_AMOPENAT_GPIO_CFG));
    output_cfg.mode = OPENAT_GPIO_OUTPUT; 
    output_cfg.param.defaultState = TRUE; 
	iot_gpio_close(10);
    iot_gpio_open(10, &output_cfg);
    NOR_FLASH_CS_H;

	return res;
}


void NorFlash_WriteRead(u8 *write, u16 wlen, u8 *read, u16 rlen)
{
    char *temp =iot_os_malloc(wlen + rlen);
    memcpy(temp, write, wlen);
    memset(temp+wlen, 0 ,rlen);
    NOR_FLASH_CS_L;
    iot_spi_rw(NOR_FLASH_SPI, (UINT8 *)temp, (UINT8 *)temp, wlen+rlen);
    NOR_FLASH_CS_H;
    memcpy(read, temp+wlen, rlen);
    iot_os_free(temp);
}


void NorFlash_Write(u8 *write1, u16 wlen1, u8 *write2, u16 wlen2)
{
    NOR_FLASH_CS_L;
    if((wlen1 != 0) && (write1 != NULL))
        iot_spi_write(NOR_FLASH_SPI, (UINT8 *)write1, wlen1);

    if((wlen2 != 0) && (write2 != NULL))
        iot_spi_write(NOR_FLASH_SPI, (UINT8 *)write2, wlen2);
    NOR_FLASH_CS_H;
}

u8 NorReadStatus(void)
{
    u8 command[2];

    command[0] = ReadStatusRegister;
    NorFlash_WriteRead(command, 1, command + 1, 1);
    return command[1];
}

void NorWriteEnable(void)
{
    u8 command[1];
    u8 data_buf = 0x00;

    command[0] = WriteEnable;

    while(1)
    {

        data_buf = NorReadStatus();
        if(data_buf & NORFLASH_WEL)
        {
            break;
        }
        else
        {
            NorFlash_WriteRead(command, 1, NULL, 0);
        }
    }
}

void NorWriteDisable(void)
{
    u8 command[1];
    u8 data_buf = 0x00;

    command[0] = WriteDisable;

    while(1)
    {

        data_buf = NorReadStatus();
        if(!(data_buf & NORFLASH_WEL))
        {
            break;
        }
        else
        {
            NorFlash_WriteRead(command, 1, NULL, 0);
        }
    }
}


void NorWaitReady(void)
{
    u8 data_buf = 0x00;

    while(1)
    {
        data_buf = NorReadStatus();
        if(!(data_buf & NORFLASH_BUSY))
        {
            break;
        }
    }
}


void NorWriteStatus(u8 Status)
{
    u8 command[2];

    command[0] = WriteStatusRegister;
    command[1] = Status;
    NorWriteEnable();
    NorFlash_WriteRead(command, 2, NULL, 0);
    NorWaitReady();
    NorWriteDisable();
}

u8 NorProtectDisable(void)
{
    u8 ucStatus, ucTemp;

    ucTemp = ucStatus = NorReadStatus();

    if(ucTemp & (0x80))
    {
        ucTemp &= (~(0x80));
        NorWriteStatus(ucTemp);
    }

    if(ucTemp & (0x1c))
    {
        ucTemp &= (~(0x1c));
        NorWriteStatus(ucTemp);
    }

    return ucStatus;
}


void NorProtectEnable(u8 ucStatus)
{
    u8 ucStatusTmp;
    u8 ucPb = (0x80 | 0x1c);

    ucStatusTmp = NorReadStatus();

    if(((ucStatusTmp ^ ucStatus) & ucPb) != 0)
    {
        if(ucStatusTmp & 0x80)
        {
            ucStatusTmp &= (~(0x80));
            NorWriteStatus(ucStatusTmp);
        }

        if(((ucStatusTmp ^ ucStatus) & ucPb) != 0)
        {
            ucStatusTmp &= (~ucPb);
            ucStatusTmp |= (ucStatus & ucPb);
            NorWriteStatus(ucStatusTmp);
        }
    }
}


#if 0

void NorSectorErase(u32 u4Addr, u32 u4Len)
{
    u32 u4EraseAddr;
    u8 pucTemp[4];

    u4EraseAddr = u4Addr;
    while(u4EraseAddr < (u4Addr + u4Len))
    {
        pucTemp[0] = SectorErase;
        pucTemp[1] = (u8)((u4EraseAddr >> 16) & 0xFF);
        pucTemp[2] = (u8)((u4EraseAddr >> 8) & 0xFF);
        pucTemp[3] = (u8)((u4EraseAddr >> 0) & 0xFF);

        NorWriteEnable();
        NorFlash_WriteRead(pucTemp, 4, NULL, 0);
        NorWaitReady();
        NorWriteDisable();

        u4EraseAddr = u4EraseAddr + NOR_SECTOR_SIZE;
    }
}

void NorBlockErase(u32 u4Addr, u32 u4Len)
{
    u32 u4EraseAddr;
    u8 pucTemp[4];

    u4EraseAddr = u4Addr;
    while(u4EraseAddr < (u4Addr + u4Len))
    {
        pucTemp[0] = BlockErase;
        pucTemp[1] = (u8)((u4EraseAddr >> 16) & 0xFF);
        pucTemp[2] = (u8)((u4EraseAddr >> 8) & 0xFF);
        pucTemp[3] = (u8)((u4EraseAddr >> 0) & 0xFF);

        NorWriteEnable();
        NorFlash_WriteRead(pucTemp, 4, NULL, 0);
        NorWaitReady();
        NorWriteDisable();

        u4EraseAddr = u4EraseAddr + NOR_BLOCK_SIZE;
    }

}
#endif


void NorAllChipErase(void)
{
    u8 ucTemp = ChipErase;

    NorWriteEnable();
    NorFlash_WriteRead(&ucTemp, 1, NULL, 0);
    NorWaitReady();
    NorWriteDisable();
}

void NorWriteData(u8 *src,u32 destAddr,u32 u4Len)
{
	u8 pucTemp[4];

	pucTemp[0] = PageProgram;
	pucTemp[1] = (u8)((destAddr >> 16) & 0xFF);
	pucTemp[2] = (u8)((destAddr >> 8) & 0xFF);
	pucTemp[3] = (u8)((destAddr >> 0) & 0xFF);

	NorWriteEnable();
	NorFlash_Write(pucTemp, 4, src, u4Len);
	NorWaitReady();
	NorWriteDisable();
}

void NorReadData(u32 srcAddr,u8 *dest,u32 u4Len)
{
	u8 pucTemp[4];

	pucTemp[0] = ReadData;
	pucTemp[1] = (u8)((srcAddr >> 16) & 0xFF);
	pucTemp[2] = (u8)((srcAddr >> 8) & 0xFF);
	pucTemp[3] = (u8)((srcAddr >> 0) & 0xFF);

	NorFlash_WriteRead(pucTemp, 4, dest, u4Len);
	NorWaitReady();
}

void ReadNorID(u8 *read_buf)
{
    u8 ins[4] = {Read_Manufacture_ID, 0x00, 0x00, 0x01};;

    // TRACE_NOTICE("[norflash]NorWaitReady\r\n");
    //NorWaitReady();
    // TRACE_NOTICE("[norflash]Nor Write cmd\r\n");
    NorFlash_WriteRead(ins, 4, read_buf, 2);
}

