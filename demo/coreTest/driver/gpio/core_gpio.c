#include "driver_comm.h"

#undef DRV_NAME
#define DRV_NAME "DRV_GPIO"

static HANDLE testTask;
static int gGpioIntCount = 0;

static VOID prvGpioHandle (E_OPENAT_DRV_EVT evt, 
                    E_AMOPENAT_GPIO_PORT gpioPort,
                unsigned char state)
{
	gGpioIntCount++;
	//DRV_PRINT("zwb prvGpioHandle %d,%d", gpioPort, gGpioIntCount);
}


static void prvTest(int gpio1, int gpio2, BOOL state)
{
	T_AMOPENAT_GPIO_CFG outCfg = {0};
	T_AMOPENAT_GPIO_CFG inCfg = {0};
	int gpioOut;
	outCfg.mode = OPENAT_GPIO_OUTPUT;
	outCfg.param.defaultState = TRUE;
	
	inCfg.mode = OPENAT_GPIO_INPUT_INT;
	inCfg.param.intCfg.debounce = 50;
	inCfg.param.intCfg.intType = OPENAT_GPIO_INT_BOTH_EDGE;
	inCfg.param.intCfg.intCb = prvGpioHandle;

	
	if (state)
	{
		iot_gpio_close(gpio1);
		iot_gpio_close(gpio2);

		iot_gpio_open(gpio1, &outCfg);
		iot_gpio_set(gpio1, 1);
		gpioOut = gpio1;
		iot_os_sleep(30);
		iot_gpio_open(gpio2, &inCfg);
	}
	else
	{
		iot_gpio_close(gpio2);
		iot_gpio_close(gpio1);

		iot_gpio_open(gpio2, &outCfg);
		iot_gpio_set(gpio2, 1);
		gpioOut = gpio2;
		iot_os_sleep(30);
		iot_gpio_open(gpio1, &inCfg);
	}

	iot_os_sleep(100);
	
	if (gGpioIntCount == 1)
	{
		DRV_PRINT_RESULT(FALSE);
	}
	
	iot_os_sleep(100);
	iot_gpio_set(gpioOut, 0);
	iot_os_sleep(100);
	iot_gpio_set(gpioOut, 1);
	iot_os_sleep(100);
	iot_gpio_set(gpioOut, 0);
	iot_os_sleep(100);
	iot_gpio_set(gpioOut, 1);
	iot_os_sleep(100);

	if (gGpioIntCount != 4)
	{
		DRV_PRINT_RESULT(FALSE);
	}
		
	gGpioIntCount = 0;
}



static void PrvTestMain(void *pParameter)
{
	int count = 0;
	BOOL state = FALSE;

	iot_pmd_poweron_ldo(OPENAT_LDO_POWER_VLCD,15);
	iot_pmd_poweron_ldo(OPENAT_LDO_POWER_MMC,15);
	iot_pmd_poweron_ldo(OPENAT_LDO_POWER_VSIM1,15);
	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		prvTest(1,4,state);
		prvTest(11,10,state);  
		prvTest(12,9,state);
		prvTest(17,5,state); 
		prvTest(13,0,state); 
		prvTest(2,3,state);
		prvTest(18,19,state); 
		prvTest(14,15,state); 
		prvTest(26,25,state);  
		prvTest(24,28,state);  
		prvTest(27,7,state);  
		prvTest(22,23,state);  
		prvTest(21,29,state);  
		prvTest(20,31,state);  
		
		iot_os_sleep(2000);
		state = !state;
	}
}

void pub_enable_gpio(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "gpio");
}

