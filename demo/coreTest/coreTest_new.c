/***************
	demo_hello
****************/

#include "iot_debug.h"
#include "iot_os.h"
#include "at_process.h"
#include "at_tok.h"


#define require(func)                    \
	extern void pub_enable_##func(void); \
	pub_enable_##func();

#define remove(func) pub_disable_##func

#define driver1_imei "866714049400293"
#define driver2_imei "862167053250001"
#define driver3_imei "862167053252106"
#define driver4_imei "862167058172135"
#define driver5_imei "862167058173703"
#define bleslave_imei "866714046612692"
#define blemaster_imei "866714044893302"
#define ds_imei "866714046619630"

char *get_imei(void)
{
	int err;
	static char imeiOut[64];
	ATResponse *p_response = NULL;
	char* line = NULL;

	at_init();

	err = at_send_command_numeric("AT+GSN", &p_response);
	if (err < 0 || p_response->success == 0){
		
		iot_debug_print("[coreTest_new] p_response->success %x err %x", p_response->success, err);
		iot_os_sleep(5000);
		iot_debug_assert(FALSE, (CHAR *)__FUNCTION__, __LINE__);
	}

	line = p_response->p_intermediates->line;
	{
		memset(imeiOut, 0, sizeof(imeiOut));
		strcpy(imeiOut,line);
	}

	iot_debug_print("[coreTest_new] imeiOut %s", imeiOut);

	at_response_free(p_response);
	return imeiOut;
}

int appimg_enter(void *param)
{
	/*设置assert模式*/
	iot_debug_set_fault_mode(OPENAT_FAULT_HANG);

	/*设置debug 信息通过MODEM口输出*/
	iot_os_set_trace_port(3);

	iot_os_sleep(5000);

	iot_debug_print("[coreTest_new] come in");
	
	char* imei = get_imei();
		
	/*添加外设1测试用例*/
	if (strcmp(imei, driver1_imei) == 0)
	{
		require(lcd);
		require(camera);
		require(pwm);
		require(uart);
		require(spi1);
		require(sd);
		
	}
	/*添加外设2测试用例*/
	else if (strcmp(imei, driver2_imei) == 0)
	{
		require(flash);
		require(adc);
		require(keypad);
		require(i2c);
		require(onewire);
	}
	else if (strcmp(imei, driver3_imei) == 0)
	{
		require(spi2);
	}
	else if (strcmp(imei, driver4_imei) == 0)
	{
		require(gpio);
	}
	/*添加蓝牙主模式测试用例*/
	else if (strcmp(imei, blemaster_imei) == 0)
	{
		require(master);
	}
	/*添加蓝牙从模式测试用例*/
	else if (strcmp(imei, bleslave_imei) == 0)
	{
		require(slave);
		//require(voiceslave);
	}
	/*添加数传测试用例*/
	if (strcmp(imei, ds_imei) == 0)
	{
		require(socket);
		require(http);
		require(mqtt);
		require(socketSsl);
		//require(ftp);
	}
	/*添加语音业务测试用例*/
	if (strcmp(imei, driver5_imei) == 0)
	{
		require(voicemaster);
	}
	return 0;
}

void appimg_exit(void)
{
	iot_debug_print("[hello]appimg_exit");
}
