/***************
	demo_oled
****************/

#include "driver_comm.h"


#undef DRV_NAME
#define DRV_NAME "DRV_ONEWIRE"
static HANDLE testTask;

static void prvTest(void)
{
	int TempNum = 0;
	char TempStr[10] = {0};

	if (DS18B20_GetTemp_Num(4, &TempNum) == 0)
    {
		DRV_PRINT("[%s,%d] TempStr %d", __FUNCTION__, __LINE__, TempNum);
    }
    iot_os_sleep(1000);
    if (DS18B20_GetTemp_String(4, &TempStr[0]) == 0)
    {
		DRV_PRINT("[%s,%d] TempStr %s", __FUNCTION__, __LINE__, TempStr);
    }
    iot_os_sleep(1000);
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		prvTest();

		iot_os_sleep(5000);
		
	}
}

void pub_enable_onewire(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "onewire");
}


