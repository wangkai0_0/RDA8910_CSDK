#include "driver_comm.h"


typedef struct
{
	UINT32 rxCount;
}uartCtx_t;


#undef DRV_NAME
#define DRV_NAME "DRV_UART"

#define UART_SEND_COUNT_MAX (5)
#define UART_SEND_BUFF "1234567789012345677890"


static uartCtx_t uartCtx[OPENAT_UART_3+1];
static HANDLE testTask;


static void prvUartCbProcess(E_AMOPENAT_UART_PORT port, T_AMOPENAT_UART_MESSAGE* evt)
{
	INT8 *recvBuff = NULL;
    int32 dataLen = evt->param.dataLen;
	int32 recvLen = 0;
	if(dataLen)
	{
		recvBuff = iot_os_malloc(dataLen+1);
		memset(recvBuff, 0, dataLen+1);
		switch(evt->evtId)
		{
		    case OPENAT_DRV_EVT_UART_RX_DATA_IND:

		        prvErrOutput((recvLen = iot_uart_read(port, (UINT8*)recvBuff, dataLen , 0)));
		       
				uartCtx[port].rxCount += recvLen;
		        break;

		    default:
		        break;
		}

		iot_os_free(recvBuff);
	}
}


static void prvUart1Cb(T_AMOPENAT_UART_MESSAGE* evt)
{
	prvUartCbProcess(OPENAT_UART_1, evt);
}

static void prvUart2Cb(T_AMOPENAT_UART_MESSAGE* evt)
{
	prvUartCbProcess(OPENAT_UART_2, evt);
}

static void prvUart3Cb(T_AMOPENAT_UART_MESSAGE* evt)
{
	prvUartCbProcess(OPENAT_UART_3, evt);
}



static void prvTest(void)
{
	T_AMOPENAT_UART_PARAM cfg;
	int i;
	static E_AMOPENAT_UART_BAUD baud[] = {
		OPENAT_UART_BAUD_1200 ,
		OPENAT_UART_BAUD_2400 ,
		OPENAT_UART_BAUD_4800 ,
		OPENAT_UART_BAUD_9600 ,
		OPENAT_UART_BAUD_14400 ,
		OPENAT_UART_BAUD_19200 ,
		OPENAT_UART_BAUD_28800 ,
		OPENAT_UART_BAUD_38400 ,
		OPENAT_UART_BAUD_57600 ,
		OPENAT_UART_BAUD_76800 ,
		OPENAT_UART_BAUD_115200 ,
		OPENAT_UART_BAUD_230400 ,
		OPENAT_UART_BAUD_460800 ,
		OPENAT_UART_BAUD_576000 ,
		OPENAT_UART_BAUD_921600 ,	
	};
	static uint32 baudIndex = 0;

	if (baudIndex >= (sizeof(baud) / sizeof(E_AMOPENAT_UART_BAUD)))
	{
		baudIndex = 0;
	}

	cfg.baud = baud[baudIndex]; //波特率
	baudIndex++;
    cfg.dataBits = 8;   //数据位
    cfg.stopBits = 1; // 停止位
    cfg.parity = OPENAT_UART_NO_PARITY; // 无校验
    cfg.flowControl = OPENAT_UART_FLOWCONTROL_NONE; //无流控
    cfg.txDoneReport = TRUE; // 设置TURE可以在回调函数中收到OPENAT_DRV_EVT_UART_TX_DONE_IND

    cfg.uartMsgHande = prvUart1Cb; //回调函数	
	prvErrOutput(iot_uart_open(OPENAT_UART_1, &cfg));

    cfg.uartMsgHande = prvUart2Cb; //回调函数	
	prvErrOutput(iot_uart_open(OPENAT_UART_2, &cfg));

    cfg.uartMsgHande = prvUart3Cb; //回调函数	
	prvErrOutput(iot_uart_open(OPENAT_UART_3, &cfg));

	DRV_PRINT("[%s,%d] cfg.baud %d", __FUNCTION__, __LINE__, cfg.baud);

	memset(&uartCtx, 0, sizeof(uartCtx));

	for (i=0; i<UART_SEND_COUNT_MAX; i++)
	{
		prvErrOutput(iot_uart_write(OPENAT_UART_1, (UINT8 *)UART_SEND_BUFF, strlen(UART_SEND_BUFF)));
		prvErrOutput(iot_uart_write(OPENAT_UART_2, (UINT8 *)UART_SEND_BUFF, strlen(UART_SEND_BUFF)));
		prvErrOutput(iot_uart_write(OPENAT_UART_3, (UINT8 *)UART_SEND_BUFF, strlen(UART_SEND_BUFF)));
		iot_os_sleep(100);
	}

	iot_os_sleep(5000);

	for (i=0; i<=OPENAT_UART_3; i++)
	{
		if (uartCtx[i].rxCount != (UART_SEND_COUNT_MAX*strlen(UART_SEND_BUFF)))
		{
			prvErrOutput(FALSE);
			DRV_PRINT("[%s,%d] port %d, rxCount %d", __FUNCTION__, __LINE__, i, uartCtx[i].rxCount);
		}
	}

	prvErrOutput(iot_uart_close(OPENAT_UART_1));
	prvErrOutput(iot_uart_close(OPENAT_UART_2));
	prvErrOutput(iot_uart_close(OPENAT_UART_3));
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		prvTest();

		iot_os_sleep(5000);
		
	}
}

void uartTest(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "uart");
}

