#include "driver_comm.h"


#undef DRV_NAME
#define DRV_NAME "DRV_ADC"

static HANDLE testTask;

static void prvTest(void)
{
	UINT32 adcValue,voltage;

	prvErrOutput(iot_adc_init(OPENAT_ADC_2, 0));
	prvErrOutput(iot_adc_init(OPENAT_ADC_3, 0));

	prvErrOutput(iot_adc_read(OPENAT_ADC_2, &adcValue, &voltage));
	DRV_PRINT("[%s,%d] adc2 adcValue %x, voltage %x", __FUNCTION__, __LINE__, adcValue, voltage);
	
	prvErrOutput(iot_adc_read(OPENAT_ADC_3, &adcValue, &voltage));
	DRV_PRINT("[%s,%d] adc3 adcValue %x, voltage %x", __FUNCTION__, __LINE__, adcValue, voltage);


	//prvErrOutput(iot_adc_close(OPENAT_ADC_2));
	//prvErrOutput(iot_adc_close(OPENAT_ADC_3));
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		prvTest();

		iot_os_sleep(5000);
		
	}
}

void adcTest(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "adc");
}
