#ifndef __DRIVER_COMM_H__
#define __DRIVER_COMM_H__

#include "stdio.h"
#include "string.h"
#include "iot_os.h"
#include "iot_debug.h"

#include "iot_adc.h"
#include "iot_uart.h"
#include "iot_i2c.h"
#include "iot_spi.h"
#include "iot_lcd.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "iot_keypad.h"
#include "iot_camera.h"
#include "iot_zbar.h"
#include "iot_fs.h"
#include "iot_pwm.h"
#include "ds18b20.h"



#define DRV_NAME "DRV"

#define DRV_PRINT(fmt, ...) iot_debug_print("["DRV_NAME"] " fmt, ##__VA_ARGS__)

#define DRV_PRINT_ERR(fmt, ...) iot_debug_print("["DRV_NAME"DERROR] " fmt, ##__VA_ARGS__)

#define DRV_PRINT_RESULT(val) do {if(!val){DRV_PRINT_ERR("[%s,%d]", __FUNCTION__, __LINE__);}}while(0)

#define DRV_TASK_PRIO (24)
#define DRV_TASK_STACK_SIZE (4096)

#define LCD_WIDTH 132
#define LCD_HEIGHT 162
#define LCD_LED (4)

void pub_enable_uart(void);

#endif
