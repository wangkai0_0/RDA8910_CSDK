#include "driver_comm.h"


#undef DRV_NAME
#define DRV_NAME "DRV_KEYPAD"

static HANDLE testTask;

static void prvKeypadCb(T_AMOPENAT_KEYPAD_MESSAGE *param)
{
	DRV_PRINT("[%s,%d] (%d,%d), bPressed %d", __FUNCTION__, __LINE__, param->data.matrix.r, param->data.matrix.c, param->bPressed);
}


static void prvTest(void)
{
	static T_AMOPENAT_KEYPAD_CONFIG cfg;
    cfg.type = OPENAT_KEYPAD_TYPE_MATRIX;
    cfg.pKeypadMessageCallback = prvKeypadCb;
    cfg.config.matrix.keyInMask = 0xf;
    cfg.config.matrix.keyOutMask = 0xf;
	
	DRV_PRINT_RESULT(iot_keypad_init(&cfg));
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	prvTest();

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		iot_os_sleep(5000);
		
	}
}

void pub_enable_keypad(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "key");
}

