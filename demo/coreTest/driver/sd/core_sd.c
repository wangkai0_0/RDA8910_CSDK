#include "driver_comm.h"


#undef DRV_NAME
#define DRV_NAME "DRV_SD"

static HANDLE testTask;

static void demo_flash_ls(char *dirName)
{
    AMOPENAT_FS_FIND_DATA findResult;
    INT32 iFd = -1;

    iFd = iot_fs_find_first(dirName, &findResult);


	DRV_PRINT("[fs] %s ls:", dirName);
    DRV_PRINT("[fs] \t%s:\t%s\t%d\t%d\t", ((findResult.st_mode&E_FS_ATTR_ARCHIVE)? "FILE":"DIR"),    
                                                findResult.st_name,
                                                findResult.st_size,
                                                findResult.mtime);

    while(iot_fs_find_next(iFd, &findResult) == 0)
    {
        DRV_PRINT("[fs] \t%s:\t%s\t%d\t%d\t", ((findResult.st_mode&E_FS_ATTR_ARCHIVE)? "FILE":"DIR"),    
                                                findResult.st_name,
                                                findResult.st_size,
                                                findResult.mtime);
    }

    if(iFd >= 0)
    {
        iot_fs_find_close(iFd);
    }
    
}


static void prvTest(void)
{
	BOOL ret = iot_fs_mount_sdcard();

	if (!ret)
	{
		DRV_PRINT_RESULT(iot_fs_format_sdcard());
		DRV_PRINT_RESULT(iot_fs_mount_sdcard());
	}

	demo_flash_ls("/sdcard0");

	DRV_PRINT_RESULT(iot_fs_umount_sdcard());
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		prvTest();
		
		iot_os_sleep(5000);
	}
}

void pub_enable_sd(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "sd");
}

