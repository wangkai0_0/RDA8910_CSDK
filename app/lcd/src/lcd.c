

#include "lcd.h"
#include "asc2_font.h"

static char *frameBuffer;

static APP_LCD_DATA sys_lcd_data = {0};

APP_LCD_DATA *LCD_GET_SYS_DATA(void)
{
	return &sys_lcd_data;
}

//设置画笔颜色
void LCD_SetPointColor(APP_LCD_PIXEL_2BYTES_COLOR color)
{
	sys_lcd_data.lcd_point_color = color;
}

//设置背景颜色
void LCD_SetBackColor(APP_LCD_PIXEL_2BYTES_COLOR color)
{
	sys_lcd_data.lcd_back_color = color;
}

//画点
//x:0~240
//y:0~320
//t:1 填充 0,清空
void LCD_DrawPoint(int x, int y, uint8_t t)
{
	if (frameBuffer == NULL)
		return;

	if (x > sys_lcd_data.lcd_width || y > sys_lcd_data.lcd_heigh)
		return; //超出范围了

	switch (sys_lcd_data.lcd_pixel_bytes)
	{
	case 2:
	{
		u16 *pPixel16 = (u16 *)frameBuffer;
		if (t)
			pPixel16[x + y * sys_lcd_data.lcd_width] = sys_lcd_data.lcd_point_color;
		else
			pPixel16[x + y * sys_lcd_data.lcd_width] = sys_lcd_data.lcd_back_color;

		break;
	}
	default:
		break;
	}
}

//画一个大点(2*2的点)
//x,y:坐标
//color:颜色
void LCD_Draw_Big_Point(u16 x, u16 y)
{
	LCD_DrawPoint(x, y, 1); //中心点
	LCD_DrawPoint(x + 1, y, 1);
	LCD_DrawPoint(x, y + 1, 1);
	LCD_DrawPoint(x + 1, y + 1, 1);
}

//在指定区域内填充画笔颜色
void LCD_Fill(T_AMOPENAT_LCD_RECT_T *rect, uint8_t mode)
{
	for (int col = rect->ltX; col < rect->rbX; col++)
	{
		for (int row = rect->ltY; row < rect->rbY; row++)
		{
			LCD_DrawPoint(col, row, mode);
		}
	}
}

//清屏函数,按照背景颜色清屏
void LCD_Clear(void)
{
	T_AMOPENAT_LCD_RECT_T rect = {0};
	rect.ltX = 0;
	rect.ltY = 0;
	rect.rbX = sys_lcd_data.lcd_width;
	rect.rbY = sys_lcd_data.lcd_heigh;
	LCD_Fill(&rect, 0);
	LCD_Refresh_Gram(); //更新显示
}

//在指定位置显示一个字符,包括部分字符
//x:0~127
//y:0~63
//size:选择字体 12/16/24
//mode:0,反白显示;1,正常显示

void LCD_ShowChar(int x, int y, uint8_t chr, uint8_t size, uint8_t mode)
{
	uint8_t temp, t, t1;
	uint8_t y0 = y;
	uint8_t csize = (size / 8 + ((size % 8) ? 1 : 0)) * (size / 2); //得到字体一个字符对应点阵集所占的字节数
	chr = chr - ' ';												//得到偏移后的值
	for (t = 0; t < csize; t++)
	{
		if (size == 12)
			temp = asc2_1206[chr][t]; //调用1206字体
		else if (size == 16)
			temp = asc2_1608[chr][t]; //调用1608字体
		else if (size == 24)
			temp = asc2_2412[chr][t]; //调用2412字体
		else
			return; //没有的字库
		for (t1 = 0; t1 < 8; t1++)
		{
			if (temp & 0x80)
				LCD_DrawPoint(x, y, mode);
			else
				LCD_DrawPoint(x, y, !mode);
			temp <<= 1;
			y++;
			if ((y - y0) == size)
			{
				y = y0;
				x++;
				break;
			}
		}
	}
}

//m^n函数
uint32_t mypow(uint8_t m, uint8_t n)
{
	uint32_t result = 1;
	while (n--)
		result *= m;
	return result;
}

//显示数字
//x,y :起点坐标
//num:数值(0~4294967295);
//len :数字的位数
//size:字体大小

void LCD_ShowNum(int x, int y, uint32_t num, uint8_t len, uint8_t size)
{
	uint8_t t, temp;
	uint8_t enshow = 0;
	for (t = 0; t < len; t++)
	{
		temp = (num / mypow(10, len - t - 1)) % 10;
		if (enshow == 0 && t < (len - 1))
		{
			if (temp == 0)
			{
				LCD_ShowChar(x + (size / 2) * t, y, ' ', size, 1);
				continue;
			}
			else
				enshow = 1;
		}
		LCD_ShowChar(x + (size / 2) * t, y, temp + '0', size, 1);
	}
}
//显示字符串
//x,y:起点坐标
//*p:字符串起始地址
//size:字体大小
void LCD_ShowString(int x, int y, const uint8_t *p, uint8_t size)
{
	while ((*p <= '~') && (*p >= ' ')) //判断是不是非法字符!
	{
		if (x > (sys_lcd_data.lcd_width - (size / 2)))
		{
			x = 0;
			y += size;
		}
		if (y > (sys_lcd_data.lcd_heigh - size))
		{
			y = x = 0;
			LCD_Clear();
		}
		LCD_ShowChar(x, y, *p, size, 1);
		x += size / 2;
		p++;
	}
}

//两点间画线功能
//x1：直线的起始x坐标
//y1：直线的起始y坐标
//x2：直线的结束x坐标
//y2：直线的结束y坐标
void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2)
{
	u16 t;
	int xerr = 0, yerr = 0, delta_x, delta_y, distance;
	int incx, incy, uRow, uCol;
	delta_x = x2 - x1; //计算坐标增量
	delta_y = y2 - y1;
	uRow = x1;
	uCol = y1;
	if (delta_x > 0)
		incx = 1; //设置单步方向
	else if (delta_x == 0)
		incx = 0; //垂直线
	else
	{
		incx = -1;
		delta_x = -delta_x;
	}
	if (delta_y > 0)
		incy = 1;
	else if (delta_y == 0)
		incy = 0; //水平线
	else
	{
		incy = -1;
		delta_y = -delta_y;
	}
	if (delta_x > delta_y)
		distance = delta_x; //选取基本增量坐标轴
	else
		distance = delta_y;
	for (t = 0; t <= distance + 1; t++) //画线输出
	{
		LCD_DrawPoint(uRow, uCol, 1); //画点
		xerr += delta_x;
		yerr += delta_y;
		if (xerr > distance)
		{
			xerr -= distance;
			uRow += incx;
		}
		if (yerr > distance)
		{
			yerr -= distance;
			uCol += incy;
		}
	}
}
//画矩形
//(x1,y1),(x2,y2):矩形的对角坐标
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2)
{
	LCD_DrawLine(x1, y1, x2, y1);
	LCD_DrawLine(x1, y1, x1, y2);
	LCD_DrawLine(x1, y2, x2, y2);
	LCD_DrawLine(x2, y1, x2, y2);
}
//在指定位置画一个指定大小的圆
//(x,y):中心点
//r    :半径
void LCD_Draw_Circle(u16 x0, u16 y0, u8 r)
{
	int a, b;
	int di;
	a = 0;
	b = r;
	di = 3 - (r << 1); //判断下个点位置的标志
	while (a <= b)
	{
		LCD_DrawPoint(x0 + a, y0 - b, 1); //5
		LCD_DrawPoint(x0 + b, y0 - a, 1); //0
		LCD_DrawPoint(x0 + b, y0 + a, 1); //4
		LCD_DrawPoint(x0 + a, y0 + b, 1); //6
		LCD_DrawPoint(x0 - a, y0 + b, 1); //1
		LCD_DrawPoint(x0 - b, y0 + a, 1);
		LCD_DrawPoint(x0 - a, y0 - b, 1); //2
		LCD_DrawPoint(x0 - b, y0 - a, 1); //7
		a++;
		//使用Bresenham算法画圆
		if (di < 0)
			di += 4 * a + 6;
		else
		{
			di += 10 + 4 * (a - b);
			b--;
		}
	}
}

//刷屏函数
void LCD_Refresh_Gram(void)
{
	if (frameBuffer == NULL)
		return;

	T_AMOPENAT_LCD_RECT_T rect = {0};
	rect.ltX = 0;
	rect.ltY = 0;
	rect.rbX = sys_lcd_data.lcd_width - 1;
	rect.rbY = sys_lcd_data.lcd_heigh - 1;
	sys_lcd_data.lcd_screen(&rect, frameBuffer);
}

//设置一些必要的函数以及参数
void LCD_Setup(APP_LCD_DATA *lcd_data)
{
	if (lcd_data == NULL)
		return;
	memcpy(&sys_lcd_data, lcd_data, sizeof(APP_LCD_DATA));
	if (frameBuffer != NULL)
		free(frameBuffer);

	int frameBufferSize = sys_lcd_data.lcd_width * sys_lcd_data.lcd_heigh * sys_lcd_data.lcd_pixel_bytes;
	frameBuffer = malloc(frameBufferSize);
	sys_lcd_data.lcd_init();
}
