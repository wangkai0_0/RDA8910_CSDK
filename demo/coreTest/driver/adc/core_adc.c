#include "driver_comm.h"


#undef DRV_NAME
#define DRV_NAME "DRV_ADC"

static HANDLE testTask;

static void prvTest(void)
{
	UINT32 adcValue,voltage;
	int chan;
	
	
	for (chan=OPENAT_ADC_1; chan<=OPENAT_ADC_5; chan++)
	{
		DRV_PRINT_RESULT(iot_adc_init(chan, 0));

		iot_pmd_poweron_ldo(OPENAT_LDO_POWER_VLCD, 15);
		
		if(iot_adc_read(chan, &adcValue, &voltage))
		{
			//DRV_PRINT("[%s,%d] chan %d, voltage %d", __FUNCTION__, __LINE__, chan, voltage);
			if (voltage > 5000 || voltage < 1000)
			{
				DRV_PRINT_RESULT(FALSE);
			}
		}

		iot_pmd_poweron_ldo(OPENAT_LDO_POWER_VLCD, 0);
		
		if(iot_adc_read(chan, &adcValue, &voltage))
		{
			//DRV_PRINT("[%s,%d] chan %d, voltage %d", __FUNCTION__, __LINE__, chan, voltage);
			if (chan == OPENAT_ADC_5)
				continue;
			
			if (voltage > 1000)
			{
				DRV_PRINT_RESULT(FALSE);
			}
		}
	}
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		prvTest();

		iot_os_sleep(5000);
		
	}
}

void pub_enable_adc(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "adc");
}
