/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-08-03 08:46:58
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */
#include "osi_log.h"
#include "osi_api.h"

#include "string.h"
#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"

#pragma GCC push_options
#pragma GCC optimize("O0")
void Delay_10us(volatile uint32 us)
{
	if (us >= 0 && us < 5)
	{
		for (volatile int i = 15 * us; i > 0; i--)
		{
			;
		}
	}
	else if (us >= 5 && us < 20)
	{
		for (volatile int i = 55 * us; i > 0; i--)
		{
			;
		}
	}
	else if (us >= 20)
	{
		for (volatile int i = 62 * us; i > 0; i--)
		{
			;
		}
	}
}

void Delay_us(volatile uint32 us)
{
	HANDLE temp = OPENAT_enter_critical_section();
	volatile int64_t usold = osiUpTimeUS();
	while (osiUpTimeUS() - usold < us)
	{
		;
	}
	OPENAT_exit_critical_section(temp);
}

#pragma GCC push_options
static void GPIO0(void *param)
{
	T_AMOPENAT_GPIO_CFG output_cfg = {0};
	output_cfg.mode = OPENAT_GPIO_OUTPUT; //配置输出
	output_cfg.param.defaultState = 0;	  // 默认低电平
	BOOL err = 0;
	// gpio0初始化
	err = iot_gpio_open(0, &output_cfg);
	if (!err)
		return;
	while (1)
	{
		//将GPIO0设置为高电平
		iot_gpio_set(0, 1); //设置为高电平
		Delay_us(1);
		//将GPIO0设置为低电平
		iot_gpio_set(0, 0); //设置为高电平
		osiThreadSleep(10);

		//将GPIO0设置为高电平
		iot_gpio_set(0, 1); //设置为高电平
		Delay_us(5);
		//将GPIO0设置为低电平
		iot_gpio_set(0, 0); //设置为高电平
		osiThreadSleep(10);

		//将GPIO0设置为高电平
		iot_gpio_set(0, 1); //设置为高电平
		Delay_us(20);
		//将GPIO0设置为低电平
		iot_gpio_set(0, 0); //设置为高电平
		osiThreadSleep(10);

		//将GPIO0设置为高电平
		iot_gpio_set(0, 1); //设置为高电平
		Delay_us(90);
		//将GPIO0设置为低电平
		iot_gpio_set(0, 0); //设置为高电平
		osiThreadSleep(10);

		//将GPIO0设置为高电平
		iot_gpio_set(0, 1); //设置为高电平
		Delay_us(100);
		//将GPIO0设置为低电平
		iot_gpio_set(0, 0); //设置为高电平
		osiThreadSleep(10);

		//将GPIO0设置为高电平
		iot_gpio_set(0, 1); //设置为高电平
		Delay_us(500);
		//将GPIO0设置为低电平
		iot_gpio_set(0, 0); //设置为高电平
		osiThreadSleep(100);
	}
	osiThreadExit();
}
// static void GPIO1(void *param)
// {
// 	T_AMOPENAT_GPIO_CFG output_cfg = {0};
// 	output_cfg.mode = OPENAT_GPIO_OUTPUT; //配置输出
// 	output_cfg.param.defaultState = 0;	  // 默认低电平
// 	BOOL err = 0;
// 	// gpio1初始化
// 	err = iot_gpio_open(1, &output_cfg);
// 	if (!err)
// 		return;
// 	while (1)
// 	{
// 		//将GPIO0设置为高电平
// 		iot_gpio_set(1, 1); //设置为高电平
// 		DS18B20_Delay_5us(1);
// 		//将GPIO0设置为低电平
// 		iot_gpio_set(1, 0); //设置为高电平
// 		osiThreadSleep(1000);
// 	}
// 	osiThreadExit();
// }
//main函数
int appimg_enter(void *param)
{
    //关闭看门狗，死机不会重启。默认打开
    iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
    //打开调试信息，默认关闭
    iot_vat_send_cmd("AT^TRACECTRL=0,1,3\r\n", sizeof("AT^TRACECTRL=0,1,3\r\n"));
    iot_debug_print("[hello]appimg_enter");
	//创建一个任务
	osiThreadCreate("GPIO0", GPIO0, NULL, OSI_PRIORITY_NORMAL, 1024, 0);
	//再创建一个任务
	//osiThreadCreate("GPIO1", GPIO1, NULL, OSI_PRIORITY_NORMAL, 1024, 0);
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
