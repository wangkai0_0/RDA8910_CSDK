#ifndef _DS_COMMON_H_
#define _DS_COMMON_H_

#include "iot_os.h"
#include "iot_debug.h"
#include "iot_network.h"
#include "iot_socket.h"

extern E_OPENAT_NETWORK_STATE g_network_state;


#define ds_error(fmt, ...)  iot_debug_print("[E] %s - %d"fmt, __FUNCTION__,__LINE__,##__VA_ARGS__)
#define ds_info(fmt, ...) iot_debug_print("[I] %s - %d"fmt, __FUNCTION__,__LINE__, ##__VA_ARGS__)




typedef enum{
	DS_SOCKET_TEST,
	DS_SOCKETSSL_TEST,
	DS_MQTT_TEST,
	DS_HTTP_TEST,
	DS_FTP_TEST,
}DS_TEST_TYPE;







#endif
