/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-05-20 10:45:57
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
//GPIO_Input

#define DS18B20_DATA 2

typedef volatile struct
{
	REG32 gpio_oen_val;			//0x00000000
	REG32 gpio_oen_set_out;		//0x00000004
	REG32 gpio_oen_set_in;		//0x00000008
	REG32 gpio_val_reg;			//0x0000000C
	REG32 gpio_set_reg;			//0x00000010
	REG32 gpio_clr_reg;			//0x00000014
	REG32 gpint_ctrl_r_set_reg; //0x00000018
	REG32 gpint_ctrl_r_clr_reg; //0x0000001C
	REG32 int_clr;				//0x00000020
	REG32 int_status;			//0x00000024
	REG32 chg_ctrl;				//0x00000028
	REG32 chg_cmd;				//0x0000002C
	REG32 gpo_set_reg;			//0x00000030
	REG32 gpo_clr_reg;			//0x00000034
	REG32 gpint_ctrl_f_set_reg; //0x00000038
	REG32 gpint_ctrl_f_clr_reg; //0x0000003C
	REG32 dbn_en_set_reg;		//0x00000040
	REG32 dbn_en_clr_reg;		//0x00000044
	REG32 gpint_mode_set_reg;	//0x00000048
	REG32 gpint_mode_clr_reg;	//0x0000004C
} DS18B20_HWP_GPIO_T;

#define REG_GPIO1_BASE 0x50107000
#define REG_ACCESS_ADDRESS(addr) (addr)
#define hwp_gpio1 ((DS18B20_HWP_GPIO_T *)REG_ACCESS_ADDRESS(REG_GPIO1_BASE))

void gpio_handle(E_OPENAT_DRV_EVT evt, E_AMOPENAT_GPIO_PORT gpioPort, unsigned char state)
{
	uint8 status = 0;
	//iot_debug_print("[gpio] gpio_handle %d,%d,%d", evt, gpioPort, state);
	// 判断是gpio中断
	if (evt == OPENAT_DRV_EVT_GPIO_INT_IND)
	{
		// 判断触发中断的管脚
		if (gpioPort == 0)
		{
			// 触发电平的状态
			iot_debug_print("[gpio] input handle gpio %d, state %d", gpioPort, state);
			// 读当前gpio状态, 1:高电平 0:低电平
			iot_gpio_read(gpioPort, &status);
			iot_debug_print("[gpio] input handle gpio %d, status %d", gpioPort, state);
		}
	}
}

static void TestTask(void *param)
{
	while (1)
	{
		uint8 status = 0;
		if (status == 0)
		{
			uint8 value = 0;
			uint8 oldstate = 0;
			DS18B20_HWP_GPIO_T *hwp = hwp_gpio1;
			hwp->gpint_ctrl_r_clr_reg = (1 << DS18B20_DATA);
			hwp->gpint_ctrl_f_clr_reg = (1 << DS18B20_DATA);
			hwp->dbn_en_clr_reg = (1 << DS18B20_DATA);
			hwp->gpint_mode_clr_reg = (1 << DS18B20_DATA);
			hwp->gpio_set_reg = (1 << DS18B20_DATA);
			hwp->gpio_oen_set_in = (1 << DS18B20_DATA);
			while (1)
			{
				value = hwp->gpio_val_reg & (1 << DS18B20_DATA);
				/* 			iot_debug_print("[gpio] input gpio1 ok");
			iot_debug_print("[gpio] value:%d,oldstate:%d", value, oldstate); */
				if (value != oldstate)
				{
					iot_debug_print("[gpio] input gpio1, state %d", value);
					oldstate = value;
				}
				osiThreadSleep(500);
				iot_gpio_read(0, &status);
				if (status != 0)
					break;
			}
		}

		DS18B20_HWP_GPIO_T *hwp = hwp_gpio1;
		hwp->gpint_ctrl_r_clr_reg = (1 << DS18B20_DATA);
		hwp->gpint_ctrl_f_clr_reg = (1 << DS18B20_DATA);
		hwp->dbn_en_clr_reg = (1 << DS18B20_DATA);
		hwp->gpint_mode_clr_reg = (1 << DS18B20_DATA);
		hwp->gpio_set_reg = (1 << DS18B20_DATA);
		hwp->gpio_oen_set_out = (1 << DS18B20_DATA);
		while (1)
		{
			hwp->gpio_set_reg = (1 << DS18B20_DATA);
			osiThreadSleep(200);
			hwp->gpio_clr_reg = (1 << DS18B20_DATA);
			osiThreadSleep(200);
			iot_gpio_read(0, &status);
			if (status != 1)
				break;
		}

		//线程休眠500ms
		osiThreadSleep(500);
	}
	osiThreadExit();
}

//main函数
int appimg_enter(void *param)
{
    //关闭看门狗，死机不会重启。默认打开
    iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
    //打开调试信息，默认关闭
    iot_vat_send_cmd("AT^TRACECTRL=0,1,3\r\n", sizeof("AT^TRACECTRL=0,1,3\r\n"));
    iot_debug_print("[hello]appimg_enter");

	T_AMOPENAT_GPIO_CFG input_cfg = {0};
	BOOL err = 0;
	input_cfg.mode = OPENAT_GPIO_INPUT_INT;						//配置输入中断
	input_cfg.param.defaultState = 0;							//默认为低电平
	input_cfg.param.intCfg.debounce = 50;						//防抖50ms
	input_cfg.param.intCfg.intType = OPENAT_GPIO_INT_BOTH_EDGE; //中断触发方式双边沿
	input_cfg.param.intCfg.intCb = gpio_handle;					//中断处理函数
	err = iot_gpio_open(0, &input_cfg);							//初始化gpio为中断输入模式
	if (!err)
		return;
	iot_debug_print("[gpio] set gpio0 INT input");

	//创建一个任务，用作查询法
	osiThreadCreate("TestTask", TestTask, NULL, OSI_PRIORITY_NORMAL, 2048, 0);
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
