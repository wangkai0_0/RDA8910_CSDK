/*
 * @Author: your name
 * @Date: 2020-05-31 14:40:33
 * @LastEditTime: 2020-05-31 16:28:12
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\http_client\http_net.c
 */

#include <stdlib.h>
#include "string.h"
#include "cs_types.h"
#include "iot_debug.h"
#include "iot_socket.h"
#include "iot_network.h"

#include "http_net.h"
#include "http_client.h"

//get报文生成，支持分段接收，数据缓存区10k
bool http_method_get_update(http_client_handle_t *client)
{
    uint8 RangeStr[50] = {0};
    uint32 splen = sprintf((char *)RangeStr, "%d-%d", client->Range_start, client->Range_end);
    //这个表示[start,end]，即是包含请求头的start及end字节的，所以，下一个请求，应该是上一个请求的[end+1, nextEnd]
    RangeStr[splen] = '\0';

    uint8 path[200] = {0};
    if (client->table_data == NULL)
    {
        uint32 len = strlen(client->path);
        memcpy(path, client->path, len);
        path[len] = '\0';
    }
    else
    {
        uint32 len = sprintf((char *)path, "%s?%s", client->path, client->table_data);
        path[len] = '\0';
    }
    memset(client->buffer_tx, 0, client->buffer_size_tx);
    splen = sprintf((char *)client->buffer_tx, GET, path, client->host, RangeStr);
    client->buffer_tx[splen] = '\0';
}

//post报文生成，数据缓存区1k
bool http_method_post_update(http_client_handle_t *client)
{
    memset(client->buffer_tx, 0, client->buffer_size_tx);

    uint32 splen = sprintf((char *)client->buffer_tx, POST, client->path, client->host, strlen(client->table_data), client->table_data);
    client->buffer_tx[splen] = '\0';
}

//对收到的响应头做出解析
bool http_Respons_Header_Analytic(http_client_handle_t *client)
{
    char *PA;
    char *PB;

    //获取回应状态
    PA = client->buffer_respons_header;
    PB = strstr(client->buffer_respons_header, "\r\n");
    memcpy(client->respons_state, PA, PB - PA);

    //获取本次传输响应体数据。recv也许一次接受不完，需要分次接收
    PA = strstr(client->buffer_respons_header, "Content-Length");
    if (PA != NULL)
    {
        PA = strstr(PA, ":");
        PB = strstr(PA, "\r\n");
        char buffer_len[20] = {0};
        memcpy(buffer_len, (PA + 1) + 1, PB - (PA + 1));
        client->buffer_rx_len = atoi(buffer_len);
    }

    //获取分段传输响应体总数据
    PA = strstr(client->buffer_respons_header, "Content-Range");
    if (PA != NULL)
    {
        PA = strstr(PA, "/");
        PB = strstr(PA, "\r\n");
        char Range_all[20] = {0};
        memcpy(Range_all, PA + 1, PB - PA);
        client->Range_all = atoi(Range_all);

        iot_debug_print("[http_client]Range_start:%d\r\n", client->Range_start);
        iot_debug_print("[http_client]Range_end:%d\r\n", client->Range_end);
        iot_debug_print("[http_client]Range_all:%d\r\n", client->Range_all);
        client->Range_start = client->Range_end + 1;                          //设置下一个滑窗请求起始地址
        client->Range_end = client->Range_start + client->buffer_size_rx - 1; //设置下一个滑窗请求结束地址
    }
}
//处理接收数据
bool http_client_recv(http_client_handle_t *client, int socketfd)
{
    uint32 datalen = client->buffer_size_rx;
    char *data = iot_os_malloc(datalen);
    memset(data, 0, datalen);
    client->buffer_rx_sum = 0;
    client->buffer_rx_len = 0;
    client->buffer_respons_header_len = 0;
    uint32 len = recv(socketfd, data, datalen, 0);
    data[len] = '\0';
    if (len < 0)
    {
        close(socketfd);
        iot_debug_print("[http_client] tcp recv data False");
        return FALSE;
    }
    else
    {
        //iot_debug_print("[http_client] tcp recv data len:%d", len);
        char *ResponsHeader = strstr(data, "\r\n\r\n"); //这个地方可能存在一个bug，就是一次接收不能接收到完整的响应头，后面就会解析失败
        //iot_debug_print("[http_client] http_loop_sent");
        while (ResponsHeader == NULL)
        {
            uint8 temp = recv(socketfd, data + len, datalen, 0);
            if (temp < 0)
            {
                close(socketfd);
                iot_debug_print("[http_client] tcp recv data False");
                return FALSE;
            }
            else
            {
                //iot_debug_print("[http_client] tcp recv data len:%d", temp);
                len += temp;
                data[len] = '\0';
                ResponsHeader = strstr(data, "\r\n\r\n");
            }
        }
        //iot_debug_print("[http_client] http_loop_sent");
        ResponsHeader += 4;
        //存储响应头
        memset(client->buffer_respons_header, 0, Respons_Header_len);
        memcpy(client->buffer_respons_header, data, ResponsHeader - data);
        //存储响应头长度
        client->buffer_respons_header_len = strlen(client->buffer_respons_header);
        http_Respons_Header_Analytic(client);
        //计算本次接收到的实际响应体长度（当这个数小于响应头附带的响应体长度时，说明需要多次接收）
        uint32 one_len = len - client->buffer_respons_header_len;
        //存储响应体
        memset(client->buffer_rx, 0, client->buffer_size_rx);
        memcpy(client->buffer_rx, ResponsHeader, one_len);
        client->buffer_rx_sum += one_len;
        //iot_debug_print("[http_client] client->buffer_rx_sum:%d", client->buffer_rx_sum);
        //iot_debug_print("[http_client] client->buffer_rx_len:%d", client->buffer_rx_len);
        //如果接收到的数据总和小于响应头附带的响应体数据大小，说明接收不完整，需要继续接收
        while (client->buffer_rx_sum < client->buffer_rx_len)
        {
            len = recv(socketfd, data, datalen, 0);
            if (len < 0)
            {
                close(socketfd);
                iot_debug_print("[http_client] tcp recv data False");
                return FALSE;
            }
            else
            {
                data[len] = '\0';
                memcpy(client->buffer_rx + client->buffer_rx_sum, data, len);
                client->buffer_rx_sum += len;
                //iot_debug_print("[http_client] client->buffer_rx_sum:%d", client->buffer_rx_sum);
            }
        }
    }
    return TRUE;
    iot_os_free(data);
}

//循环发送
bool http_loop_sent(http_client_handle_t *client, int socketfd)
{
    if (client->method == HTTP_METHOD_GET)
    {
        http_method_get_update(client);
    }
    else
    {
        if (client->method == HTTP_METHOD_POST)
        {
            http_method_post_update(client);
        }
    }
    // TCP 发送数据
    int len = send(socketfd, client->buffer_tx, strlen(client->buffer_tx), 0);
    if (len < 0)
    {
        iot_debug_print("[http_client] tcp send data False");
        close(socketfd);
    }
    else
    {
        if (http_client_recv(client, socketfd) == FALSE)
        {
            http_client_event evt = {0};
            evt.state = HTTP_FALSE;
            if (client->event_handler)
            {
                client->event_handler(&evt);
            }
            return FALSE;
        }
        http_client_event evt = {0};
        evt.state = HTTP_TRUE;
        evt.respons_state = client->respons_state;
        evt.data = client->buffer_rx;
        evt.datalen = client->buffer_rx_sum;
        evt.respons_state = client->respons_state;
        if (client->event_handler)
        {
            client->event_handler(&evt);
        }

        return TRUE;
    }
}

//发送请求任务
void http_client_perform_task(void *param)
{
    http_client_handle_t *client = (http_client_handle_t *)param;
    //创建套接字
    int socketfd = socket(OPENAT_AF_INET, OPENAT_SOCK_STREAM, 0);
    if (socketfd < 0)
    {
        iot_debug_print("[http_client] create tcp socket error");
    }
    else
    {
        // 建立TCP链接
        struct openat_sockaddr_in tcp_server_addr = {0};
        //AF_INET 的目的就是使用 IPv4 进行通信
        tcp_server_addr.sin_family = OPENAT_AF_INET;
        //远端端口，主机字节顺序转变成网络字节顺序
        tcp_server_addr.sin_port = htons((unsigned short)client->port);
        //字符串远端ip转化为网络序列ip
        inet_aton(client->ip, &tcp_server_addr.sin_addr);
        int connErr = connect(socketfd, (const struct openat_sockaddr *)&tcp_server_addr, sizeof(struct openat_sockaddr));
        if (connErr < 0)
        {
            iot_debug_print("[http_client] tcp connect error %d", socket_errno(socketfd));
            close(socketfd);
        }
        else
        {
            http_loop_sent(client, socketfd);
            while (client->Range_start < client->Range_all)
            {
                http_loop_sent(client, socketfd);
            }
            /*             iot_os_sleep(3000);
            iot_debug_print("[http_client] Get OK");
            iot_os_sleep(3000);
            iot_debug_print("[http_client] Get OK"); */
        }
    }

    iot_os_delete_task(client->perform_task_handle);
}
