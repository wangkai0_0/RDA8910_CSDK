#include <stdio.h>
#include <string.h>
#include "iot_debug.h"
#include "iot_os.h"
#include "iot_audio.h"
#include "iot_tts.h"
#include "iot_lcd.h"
#include "iot_fs.h"

#include "am_openat_sms.h"
#include "am_openat_drv.h"
#include "am_openat_tts.h"
#include "at_process.h"
#include "at_tok.h"


#define VOICE_SUSPENDED (0xFFFFFFFF)
#define VOICE_TASK_PRIO (24)
#define VOICE_TASK_STACK_SIZE (1024*10)
#define VOICE_IMAGE_TASK_STACK_SIZE (1024*4)
#define VOICE_PLAY_SIZE  1200
#define RECSTREAM_TIMER_TIMEOUT 5000
#define FILE_NAME "record.amr"

#define VOICE_NAME "voicemaster"
#define VOICE_PRINT(fmt, ...) iot_debug_print("["VOICE_NAME"] " fmt, ##__VA_ARGS__)
#define VOICE_PRINT_ERR(fmt, ...) iot_debug_print("["VOICE_NAME"DERROR] " fmt, ##__VA_ARGS__)
#define VOICE_PRINT_RESULT(val) do {if(!val){VOICE_PRINT_ERR("[%s,%d]", __FUNCTION__, __LINE__);}}while(0)


#define LCD_WIDTHT 132
#define LCD_HEIGHT 162
#define LCD_LED (4)


static HANDLE test_handle;
static HANDLE tts_handle;
static HANDLE image_hanle;
static HANDLE g_timer;

static HANDLE allover_sem;
static HANDLE tts_sem;

static char masterNum[15] = "19531220708";
static char lcd_buf[LCD_WIDTHT*LCD_HEIGHT*2]={0xFF};
static char image_buf[LCD_WIDTHT*LCD_HEIGHT*2]={0};

static const UINT32 lcdRegTable[] = 
{
	0x00020011,
	0x00010078,
	0x000200B1,
	0x00030002,
	0x00030035,
	0x00030036,
	0x000200B2,
	0x00030002,
	0x00030035,
	0x00030036,
	0x000200B3,
	0x00030002,
	0x00030035,
	0x00030036,
	0x00030002,
	0x00030035,
	0x00030036,
	0x000200B4,
	0x00030007,
	0x000200C0,
	0x000300A2,
	0x00030002,
	0x00030084,
	0x000200C1,
	0x000300C5,
	0x000200C2,
	0x0003000A,
	0x00030000,
	0x000200C3,
	0x0003008A,
	0x0003002A,
	0x000200C4,
	0x0003008A,
	0x000300EE,
	0x000200C5,
	0x0003000E,
	0x00020036,
	0x000300C0,
	0x000200E0,
	0x00030012,
	0x0003001C,
	0x00030010,
	0x00030018,
	0x00030033,
	0x0003002C,
	0x00030025,
	0x00030028,
	0x00030028,
	0x00030027,
	0x0003002F,
	0x0003003C,
	0x00030000,
	0x00030003,
	0x00030003,
	0x00030010,
	0x000200E1,
	0x00030012,
	0x0003001C,
	0x00030010,
	0x00030018,
	0x0003002D,
	0x00030028,
	0x00030023,
	0x00030028,
	0x00030028,
	0x00030026,
	0x0003002F,
	0x0003003B,
	0x00030000,
	0x00030003,
	0x00030003,
	0x00030010,
	0x0002003A,
	0x00030005,
	0x00020029,
};

static void write_command_table(const UINT32 *table, UINT16 size)
{
    UINT16 flag;
    UINT16 value;
    UINT16 index;

    for(index = 0; index < size && table[index] != (UINT32)-1; index++)
    {
        flag = table[index]>>16;
        value = table[index]&0xffff;

        switch(flag)
        {
            case 1:
                iot_os_sleep(value);
                break;

            case 0:
            case 2:
                iot_lcd_write_cmd(value&0x00ff);
                break;

            case 3:
                iot_lcd_write_data(value&0x00ff);
                break;

            default:
                break;
        }
    }
}

static BOOL prvLcdInit(void)
{
	T_AMOPENAT_COLOR_LCD_PARAM param;

	VOICE_PRINT_RESULT(iot_pmd_poweron_ldo(OPENAT_LDO_POWER_VLCD,15));

	memset(&param, 0, sizeof(param));
    param.width = LCD_WIDTHT;
    param.height = LCD_HEIGHT;
    param.msgCallback = NULL;
    param.bus = OPENAT_LCD_SPI4LINE; 
    VOICE_PRINT_RESULT(iot_lcd_color_init(&param));
	
	write_command_table(lcdRegTable, sizeof(lcdRegTable)/sizeof(int));
	
	return TRUE;
}


static void tts_play_cb(OPENAT_TTS_CB_MSG msg_id,u8 event)
{
	VOICE_PRINT_RESULT(msg_id != OPENAT_TTS_CB_MSG_ERROR);
	//iot_os_release_semaphore(tts_sem);
}

static VOID tts_test(VOID)
{
	T_AMOPENAT_SYSTEM_DATETIME* pDatetime;
  	pDatetime = iot_os_malloc(sizeof(T_AMOPENAT_SYSTEM_DATETIME));
	static char ttsStr[128] = {0};
	iot_tts_init(tts_play_cb);
	iot_tts_set_param(OPENAT_TTS_PARAM_CODEPAGE,OPENAT_CODEPAGE_GBK);
	//tts_sem = iot_os_create_semaphore(1);

  	iot_os_get_system_datetime(pDatetime);
	if((pDatetime->nHour >= 9) && (pDatetime->nHour < 11))
		sprintf(ttsStr, "当前北京时间:%d年%d月%d日,%d点%d分%d秒,上班了,记得及时打卡",pDatetime->nYear,pDatetime->nMonth,pDatetime->nDay,pDatetime->nHour,pDatetime->nMin,pDatetime->nSec);
    else if ((pDatetime->nHour >= 11) && (pDatetime->nHour < 21))
		sprintf(ttsStr, "当前北京时间:%d年%d月%d日,%d点%d分%d秒",pDatetime->nYear,pDatetime->nMonth,pDatetime->nDay,pDatetime->nHour,pDatetime->nMin,pDatetime->nSec);
    else
		sprintf(ttsStr, "当前北京时间:%d年%d月%d日,%d点%d分%d秒,下班了,不要忘记打卡",pDatetime->nYear,pDatetime->nMonth,pDatetime->nDay,pDatetime->nHour,pDatetime->nMin,pDatetime->nSec);
	
	//iot_os_wait_semaphore(tts_sem,VOICE_SUSPENDED);
	VOICE_PRINT_RESULT(iot_tts_play(ttsStr,strlen(ttsStr)));
	memset(ttsStr, 0, 128);
  	iot_os_free(pDatetime);	
}

static BOOL set_CSCS(char* type)	
{
	int err;
	ATResponse *p_response = NULL;
	bool result = FALSE;
	char cmd[20] = {0};
	sprintf(cmd, "AT+CSCS=%s", type);
	
	err = at_send_command(cmd, &p_response);
	if (err < 0 || p_response->success == 0){
		result = FALSE;
		goto end;
	}

	result = TRUE;
	end:              
		at_response_free(p_response);
		return result;
}

static BOOL pb_setstorage(char* type)	
{
	int err;
	ATResponse *p_response = NULL;
	bool result = FALSE;
	char cmd[20] = {0};
	sprintf(cmd, "AT+CPBS=%s", type);
	
	err = at_send_command(cmd, &p_response);
	if (err < 0 || p_response->success == 0){
		result = FALSE;
		goto end;
	}

	result = TRUE;
	end:              
		at_response_free(p_response);
		return result;
}

static BOOL pb_delete(int index)	
{
	int err;
	ATResponse *p_response = NULL;
	bool result = FALSE;
	char cmd[20] = {0};
	sprintf(cmd, "AT+CPBW=%d", index);
	
	err = at_send_command(cmd, &p_response);
	if (err < 0 || p_response->success == 0){
		result = FALSE;
		goto end;
	}

	result = TRUE;
	end:			  
		at_response_free(p_response);
		return result;

}

static BOOL pb_read(int index, char* name, char* num)	
{
	int err;
	ATResponse *p_response = NULL;
	char* line = NULL;
	bool result = FALSE;

	char *p_out = NULL;
    int temp;
	
	char cmd[20] = {0};
	sprintf(cmd, "AT+CPBR=%d", index);	
	
	err = at_send_command_singleline(cmd, "+CPBR:", &p_response);
	if (err < 0 || p_response->success == 0){
		result = FALSE;
		goto end;
	}
	line = p_response->p_intermediates->line;  
	
	err = at_tok_start(&line);
	if (err < 0){
		goto end;
	}
	//index
	err = at_tok_nextint(&line,&temp);
	if (err < 0){
		goto end;
	}
	//num
	err = at_tok_nextstr(&line,&p_out);
	if (err < 0){
		goto end;
	}
	strcpy(num,p_out);
	
	err = at_tok_nextint(&line,&temp);
	if (err < 0){
		goto end;
	}
	//name
	err = at_tok_nextstr(&line,&p_out);
	if (err < 0){
		goto end;
	}
	strcpy(name,p_out);

	result = TRUE;
	end:			  
		at_response_free(p_response);
		return result;

}

static BOOL pb_write(int index, char* name, char* num)	
{
	int err;
	ATResponse *p_response = NULL;
	char* line = NULL;
	bool result = FALSE;
	char cmd[200] = {0};
	sprintf(cmd, "AT+CPBW=%d,\"%s\",129,\"%s\"", index, num, name);	
	
	err = at_send_command(cmd, &p_response);
	if (err < 0 || p_response->success == 0){
		result = FALSE;
		goto end;
	}

	result = TRUE;
	end:			  
		at_response_free(p_response);
		return result;

}

static void pb_test(int index)
{
	char num[15] = {0};
	char name[25] = {0};
	VOICE_PRINT_RESULT(pb_setstorage("SM"));
	iot_os_sleep(5000);
	VOICE_PRINT_RESULT(pb_delete(index));
	iot_os_sleep(5000);
	VOICE_PRINT_RESULT(pb_write(index, "luatest", "1234567890"));
	iot_os_sleep(5000);
	VOICE_PRINT_RESULT(pb_read(index, name, num));
	if(!strcmp(num,"1234567890") && !strcmp(name, "luatest"))
		VOICE_PRINT_RESULT(1);
	else
		VOICE_PRINT_RESULT(0);
}

static BOOL recordstream_fs_init(char* file)
{
    INT32 fd;

    fd = iot_fs_open_file(file, FS_O_RDONLY);

    if (fd >= 0) //FILE_NAME文件存在，就删除重新创建
    {
        iot_fs_delete_file(file);
    }
    
    // 创建文件FILE_NAME
    iot_fs_create_file(file);

    iot_fs_close_file(fd);

    return TRUE;
}

static BOOL recordstream_fs_write(char* file, char *write_buff, INT32 len)
{
    INT32 fd;
    INT32 write_len = 0;
	
    fd = iot_fs_open_file(file, FS_O_RDWR);

    if (fd < 0)
        return FALSE;
	
    iot_fs_seek_file(fd, 0, FS_SEEK_END);
	
    write_len = iot_fs_write_file(fd, (UINT8 *)write_buff, len);

    if (write_len < 0)
        return FALSE;
    

    iot_fs_close_file(fd);
	return TRUE;
}

static int fs_size(void* f){
    INT32 fd = (INT32)f;
    int pos = iot_fs_seek_file(fd,0,FS_SEEK_CUR);
    int offset = iot_fs_seek_file(fd,0,FS_SEEK_END);
    iot_fs_seek_file(fd,pos,FS_SEEK_SET);
    return offset;
}
static int fs_read(void* buf,int size,void* f){
    INT32 fd = (INT32)f;
    int readed_size = iot_fs_read_file(fd, buf, size);
    return readed_size;
}
static void fs_close(void* f){
    INT32 fd = (INT32)f;
    iot_fs_close_file(fd);
}

static void* fs_open(const char* name ,int read,int write){
    void* fd = 0;
    UINT32 flags = 0;
    if(read){
        flags |= FS_O_RDONLY;
    }
    if(write){
        flags |= FS_O_WRONLY|FS_O_CREAT;
    }
    fd = iot_fs_open_file(name, flags);
    return fd;
}

static char* read_iamgefile(const char* fname,int* plen)
{
    void* f = fs_open(fname,1,0);
    if(!f){
        return 0;
    }
    *plen = 0;
    int len = fs_size(f);
	memset(image_buf, 0, LCD_WIDTHT*LCD_HEIGHT*2);
    char* buf = (char*)image_buf;
    fs_read(buf,len,f);
    *plen = len;
	fs_close(f);
    return buf;
}

static char* read_file(const char* fname,int* plen)
{
    void* f = fs_open(fname,1,0);
    if(!f){
        return 0;
    }
    *plen = 0;
    int len = fs_size(f);
     char* buf = (char*)iot_os_malloc(len);
    fs_read(buf,len,f);
    *plen = len;
	fs_close(f);
    return buf;
}


static BOOL display_image(const char* fname)
{
	int len = 0;
	char *imagebuf = NULL;
	T_AMOPENAT_IMAGE_INFO imageinfo = {0};
	T_AMOPENAT_LCD_RECT_T rect = {0};
	rect.ltX = 0;
    rect.ltY = 0;
    rect.rbX = LCD_WIDTHT;
    rect.rbY = LCD_HEIGHT;
	imagebuf = read_iamgefile(fname, &len);
	if(imagebuf == NULL)
	{
		VOICE_PRINT_RESULT(0);
		return FALSE;
	}
	if(!iot_decode_jpeg(imagebuf, len, &imageinfo))
	{
		memset(lcd_buf,0xFF,LCD_WIDTHT*LCD_HEIGHT*2);
		u16 *pPixel16 = (u16 *)lcd_buf;
		u16 row = 0;
		u16 col = 0;
        if(imageinfo.height > LCD_HEIGHT)  
			imageinfo.height = LCD_HEIGHT;
		if(imageinfo.width > LCD_WIDTHT)  
			imageinfo.width = LCD_WIDTHT;
        for (int i = 0; i < len; i++)
        {
        	pPixel16[col + row * LCD_WIDTHT] = imageinfo.buffer[i];
			col++;
			if(col >= imageinfo.width)
			{
				col = 0;
				row++;
			}
			if(row >= imageinfo.height)
			{
				break;
			}
        }
		iot_lcd_update_color_screen(&rect, (u16 *)lcd_buf);
	}
	else
		VOICE_PRINT_RESULT(0);
	
	iot_free_jpeg_decodedata(&imageinfo);
}

static VOID test_dispimage(PVOID pParameter)
{
	while(1)
	{
		display_image("/sdcard0/logo_50x50.jpg");
		iot_os_sleep(10000);
		display_image("/sdcard0/logo_128x160.jpg");
		iot_os_sleep(10000);
	}
	iot_os_delete_task(image_hanle);
}

VOID audio_paly_handle(E_AMOPENAT_PLAY_ERROR result)
{
	VOICE_PRINT_RESULT(result == OPENAT_AUD_PLAY_ERR_NO);
}

VOID stream_paly_handle(E_AMOPENAT_PLAY_ERROR result)
{
	VOICE_PRINT_RESULT(result == OPENAT_AUD_PLAY_ERR_NO);
}

static VOID test_audio(const char* fname, E_AMOPENAT_AUD_FORMAT format)
{
	T_AMOPENAT_PLAY_PARAM playParam;
	playParam.playBuffer = FALSE;
    playParam.playFileParam.callback = audio_paly_handle;
    playParam.playFileParam.fileName = fname;
	playParam.playFileParam.fileFormat = format;
    VOICE_PRINT_RESULT(!iot_audio_play_music(&playParam));
}

static BOOL test_streamplay(const char* fname, E_AMOPENAT_AUD_FORMAT format)
{
	int pcm_size = 0;
	char* pcmbuf = 0;
	int len;
	int plen;
	char* p = NULL;
	pcm_size = 0;
	pcmbuf = NULL;
    pcmbuf = read_file(fname,&pcm_size);
    if(!pcmbuf){
        return FALSE;
    }

    p = pcmbuf;
	len = 0;
	plen = 0;
	while(plen < pcm_size)
	{
		len = iot_audio_streamplay(format, stream_paly_handle, p, VOICE_PLAY_SIZE);
		if(len == -1)
			return FALSE;
		p += len;
		plen += len;
	}
	iot_audio_stop_music();
	iot_os_free(pcmbuf);
	iot_os_sleep(2000);
	return TRUE;
}

static void audio_rec_handle(E_AMOPENAT_RECORD_ERROR result)
{
    VOICE_PRINT_RESULT(result == OPENAT_AUD_REC_ERR_NONE);
}

static void audio_stream_record_cb(int event,char* data,int len)
{
	//将流录音数据写到文件FILE_NAME
	recordstream_fs_write(FILE_NAME, data, len);	
}

static VOID time_handle(T_AMOPENAT_TIMER_PARAMETER *pParameter)
{
	iot_os_stop_timer(g_timer);
	if(g_timer)
		iot_os_delete_timer(g_timer);
	VOICE_PRINT_RESULT(iot_audio_rec_stop());
	test_audio(FILE_NAME, OPENAT_AUD_FORMAT_AMRNB);
}

static VOID test_audRecStreamStart(VOID)
{
    BOOL err = FALSE;
	E_AMOPENAT_RECORD_PARAM param;
	recordstream_fs_init(FILE_NAME);
	param.record_mode = OPENAT_RECORD_STREAM;
	param.quality = (E_AMOPENAT_RECORD_QUALITY)1;
	param.type = (E_AMOPENAT_RECORD_TYPE)1;
	param.format = OPENAT_AUD_FORMAT_AMRNB;
	param.time_sec = 0;
	param.stream_record_cb = audio_stream_record_cb;
    VOICE_PRINT_RESULT(iot_audio_rec_start(&param,audio_rec_handle));
	g_timer = iot_os_create_timer((PTIMER_EXPFUNC)time_handle, NULL);
    iot_os_start_timer(g_timer, RECSTREAM_TIMER_TIMEOUT);
}

static void image_test()
{
	image_hanle = iot_os_create_task(test_dispimage, NULL,VOICE_IMAGE_TASK_STACK_SIZE, VOICE_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "test_dispimage");
}

static void voicemaster_test(PVOID pParameter)
{
	static u8 cnt = 1;
	u8 pb_index = 1;
	E_AMOPENAT_TONE_TYPE tone = 0;
	E_AMOPENAT_DTMF_TYPE dtmf = 0;
	BOOL ret = 0;
	iot_os_sleep(1000);
	ret = iot_fs_mount_sdcard();
	if (!ret)
	{
		VOICE_PRINT_RESULT(iot_fs_format_sdcard());
		VOICE_PRINT_RESULT(iot_fs_mount_sdcard());
	}
	iot_os_sleep(1000);
	/*image测试*/
	image_test();
	while(1)
	{
		iot_os_sleep(1000);
		/*tts测试*/
		tts_test();
		iot_os_sleep(30000);
		if(cnt < 3)
		{
			if(cnt%2 == 1)
			{
				VOICE_PRINT_RESULT(sms_send( masterNum, "1234567890"));
			}
			else
			{
				VOICE_PRINT_RESULT(sms_send(masterNum, "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"));
			}
		}
		iot_os_sleep(30000);
		cnt++;
		/*pb测试*/
		VOICE_PRINT_RESULT(set_CSCS("PCCP936"));
		iot_os_sleep(1000);
		pb_test(pb_index);
		iot_os_sleep(1000);
		VOICE_PRINT_RESULT(set_CSCS("UCS2"));
		iot_os_sleep(1000);
		pb_index = (pb_index == 50) ? 1 : (pb_index+1);
		/*tone播放*/
		for(tone = 0; tone < OPENAT_AUD_TONE_END; tone++)
		{
			iot_audio_play_tone(tone, 500, OPENAT_AUD_SPK_GAIN_3dB);
			iot_os_sleep(2000);
			iot_audio_stop_tone();
		}
		/*dtmf播放*/
		for(dtmf = 0; dtmf < OPENAT_AUD_TONE_DTMF_END; dtmf++)
		{
			iot_audio_play_dtmf(dtmf, 500, OPENAT_AUD_SPK_GAIN_3dB);
			iot_os_sleep(2000);
			iot_audio_stop_dtmf();
		}
		
		/*音频播放*/
		iot_os_sleep(2000);
		VOICE_PRINT_RESULT(iot_audio_set_channel(OPENAT_AUDIOHAL_ITF_RECEIVER));
		test_audio("/sdcard0/tip.amr", OPENAT_AUD_FORMAT_AMRNB);
		iot_os_sleep(30000);

		iot_audio_stop_music();
		VOICE_PRINT_RESULT(iot_audio_set_channel(OPENAT_AUDIOHAL_ITF_EARPIECE));
		test_audio("/sdcard0/alarm_door.pcm", OPENAT_AUD_FORMAT_PCM);
		iot_os_sleep(30000);

		iot_audio_stop_music();
		VOICE_PRINT_RESULT(iot_audio_set_channel(OPENAT_AUDIOHAL_ITF_LOUDSPEAKER));
		iot_audio_set_speaker_vol(70);
		test_audio("/sdcard0/sms.mp3", OPENAT_AUD_FORMAT_MP3);
		iot_os_sleep(30000);

		iot_audio_stop_music();
		test_audio("/sdcard0/record.spx", OPENAT_AUD_FORMAT_SPEEX);
		iot_os_sleep(30000);
		
		/*流播放*/
		VOICE_PRINT_RESULT(iot_audio_set_channel(OPENAT_AUDIOHAL_ITF_RECEIVER));
		VOICE_PRINT_RESULT(test_streamplay("/sdcard0/tip.amr", OPENAT_AUD_FORMAT_AMRNB));
		iot_os_sleep(30000);

		VOICE_PRINT_RESULT(iot_audio_set_channel(OPENAT_AUDIOHAL_ITF_EARPIECE));
		VOICE_PRINT_RESULT(test_streamplay("/sdcard0/alarm_door.pcm", OPENAT_AUD_FORMAT_PCM));
		iot_os_sleep(30000);

		VOICE_PRINT_RESULT(iot_audio_set_channel(OPENAT_AUDIOHAL_ITF_LOUDSPEAKER));
		iot_audio_set_speaker_vol(80);
		VOICE_PRINT_RESULT(test_streamplay("/sdcard0/sms.mp3", OPENAT_AUD_FORMAT_MP3));
		iot_os_sleep(30000);

		/*流录音*/
		test_audRecStreamStart();
		iot_os_sleep(30000);
		iot_audio_stop_music();
		
	}
	VOICE_PRINT_RESULT(iot_fs_umount_sdcard());
}

void pub_enable_voicemaster(void)
{
	VOICE_PRINT("test start");
	iot_debug_set_fault_mode(1);
	sms_init();
	prvLcdInit();
	
    test_handle = iot_os_create_task(voicemaster_test, NULL, VOICE_TASK_STACK_SIZE, VOICE_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "voicemaster");
}
