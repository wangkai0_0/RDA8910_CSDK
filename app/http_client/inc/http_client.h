/*
 * @Author: your name
 * @Date: 2020-05-27 10:45:08
 * @LastEditTime: 2020-05-31 17:25:43
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\app\http_client\inc\http_client.h
 */
#ifndef APP_HTTP_CLIENT_H
#define APP_HTTP_CLIENT_H

//默认接收缓存区大小
#define BUFFER_RX 10240
//默认发送缓存区大小
#define BUFFER_TX 1024
//默认响应头长度
#define Respons_Header_len 1536 //1536
//响应头状态长度
#define respons_state_len 30

typedef enum
{
    HTTP_FALSE = 0, //本次请求发生错误，附带数据无效，需要重新发送请求，
    HTTP_TRUE,      //本次请求正确，附带数据有效
} http_client_event_state;
typedef struct
{
    http_client_event_state state; //本次请求的状态，设备有没有发送错误或者接受错误。
    char *respons_state;           //本次请求的响应状态，服务器回应状态
    char *data;                    //本次请求接收到的数据
    uint32 datalen;                //本次请求接收到的数据长度

} http_client_event;

typedef void (*http_event_handle_cb)(http_client_event *evt);

/**
 * @brief HTTP方法
 */
typedef enum
{
    HTTP_METHOD_GET = 0, /*!< HTTP GET 方法 */
    HTTP_METHOD_POST,    /*!< HTTP POST 方法 */
} http_client_method_t;

typedef enum
{
    HTTP_NO_URL = 0,       //没有找到url、host、ip
    HTTP_NO_POST,          //没有找到port
    HTTP_NO_IP,            //DNS获取ip失败
    HTTP_NO_EVENT_HANDLER, //没有找到事件回调函数
    HTTP_INIT_OK           //初始化成功
} http_client_init_state;

typedef struct
{
    char *url;                          /*!< <HTTP URL，URL上的信息最重要，它会覆盖下面的其他字段（如果有） */
    http_client_method_t method;        /*!< HTTP方法 */
    http_event_handle_cb event_handler; /*!< HTTP事件处理  */
    char *host;                         /*!< <域名字符串 */
    char *ip;                           /*!< <ip字符串 */
    int port;                           /*!< 用于连接的端口（80或443） */
    char *path;                         /*!< HTTP文件路径，如果未设置，默认为`/` */
    char *table_data;                   /*!< HTTP 附加的表单数据 */
    int buffer_size_rx;                 /*!< HTTP接收缓冲区大小，默认10240字节(10k)，不宜过大，创建对象后不可修改 */
    int buffer_size_tx;                 /*!< HTTP发送缓冲区大小，默认1024字节(1k)，创建对象后不可修改 */
} http_client_config_t;

typedef struct
{
    http_client_init_state init_state;  /*!< HTTP初始化状态 */
    char *host;                         /*!< <域名字符串 */
    char *ip;                           /*!< <ip字符串 */
    int port;                           /*!< 用于连接的端口（80或443） */
    char *path;                         /*!< HTTP文件路径，如果未设置，默认为`/` */
    char *table_data;                   /*!< HTTP 附加的表单数据 */
    http_client_method_t method;        /*!< HTTP方法 */
    http_event_handle_cb event_handler; /*!< HTTP事件处理  */
    int buffer_size_tx;                 /*!< HTTP发送缓冲区大小，默认1024字节(1k) */
    char *buffer_tx;                    //发送数据缓存区，默认1024字节(1k)
    int buffer_size_rx;                 //HTTP接收缓冲区大小，默认10240字节(10k)
    char *buffer_rx;                    //接收数据缓存区，默认10240字节(10k)
    uint32 buffer_rx_len;               //保存接收到的响应体长度
    uint32 buffer_rx_sum;               //保存本次请求所接收到的数据和，可能一次请求会分多次接收。若接收到的和小于响应体长度，那就需要再次接收

    char *buffer_respons_header;      //用于存储响应头缓存区，默认4096字节(4k)。用户不可配置，如有必要请在http_client.c中更改宏定义
    uint32 buffer_respons_header_len; //保存接收到的响应头长度
    uint32 Range_start;               //上一次滑窗开始字节地址
    uint32 Range_end;                 //上一次滑窗结束字节地址
    uint32 Range_all;                 //本次请求的文件整个文件大小（不是单次接收到的数据量，是即将进行的多次接收的总数据量）
    char *respons_state;              //本次请求的状态，正常，HTTP/1.1 200 OK。分段传输：HTTP/1.1 206 Partial Content
    HANDLE perform_task_handle;

    //    const char *username;               /*!< 用于Http认证 */
    //    const char *password;               /*!< 用于Http认证 */
    //    const char *cert_pem;               /*!< SSL服务器认证，PEM格式为字符串（如果客户端要求验证服务器） */
    //    const char *client_cert_pem;        /*!< SSL客户端认证，PEM格式为字符串（如果服务器要求验证客户端） */
    //    const char *client_key_pem;         /*!< SSL客户端密钥，PEM格式为字符串，如果服务器需要验证客户端 */
} http_client_handle_t;

//修改host数据，必须先创建一个client对象才可以修改
void http_set_host(http_client_handle_t *client, char *host);
//获取host数据
char *http_get_host(http_client_handle_t *client);

//修改ip数据，必须先创建一个client对象才可以修改
void http_set_ip(http_client_handle_t *client, char *ip);
//获取ip数据
char *http_get_ip(http_client_handle_t *client);

//修改port数据，必须先创建一个client对象才可以修改
void http_set_port(http_client_handle_t *client, int port);
//获取port数据
int http_get_port(http_client_handle_t *client);

//修改path数据，必须先创建一个client对象才可以修改
void http_set_path(http_client_handle_t *client, char *path);
//获取path数据
char *http_get_path(http_client_handle_t *client);


//修改table_data数据，必须先创建一个client对象才可以修改
void http_set_table_data(http_client_handle_t *client, char *table_data);
//获取table_data数据
char *http_get_table_data(http_client_handle_t *client);

//修改method数据，必须先创建一个client对象才可以修改
void http_set_method(http_client_handle_t *client, http_client_method_t method);
//获取method数据
http_client_method_t http_get_method(http_client_handle_t *client);

//修改event_handler数据，必须先创建一个client对象才可以修改
void http_set_event_handler(http_client_handle_t *client, http_event_handle_cb event_handler);


//删除http对象
void http_delete(http_client_handle_t *client);
//获取初始化状态
http_client_init_state http_get_init_state(http_client_handle_t *client);
//新建一个http的对象
http_client_handle_t http_client_new(http_client_config_t *config);
//根据新建的对象，执行相应的操作
void http_client_perform(http_client_handle_t *client);
#endif