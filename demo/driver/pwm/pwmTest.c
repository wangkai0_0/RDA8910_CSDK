#include "driver_comm.h"
#include "am_openat.h"

#undef DRV_NAME
#define DRV_NAME "DRV_PWM"

static HANDLE testTask;


static void prvTest(void)
{
	T_AMOPENAT_PWM_CFG pwm_cfg;

	prvErrOutput(iot_pwm_open(OPENAT_PWM_PWT_OUT));
	pwm_cfg.port = OPENAT_PWM_PWT_OUT;
	pwm_cfg.cfg.pwt.freq = 300;
	pwm_cfg.cfg.pwt.level = 300;
	prvErrOutput(iot_pwm_set(&pwm_cfg));

	prvErrOutput(iot_pwm_open(OPENAT_PWM_LPG_OUT));
	pwm_cfg.port = OPENAT_PWM_LPG_OUT;
	pwm_cfg.cfg.lpg.period = PWM_LGP_PER_125MS;
	pwm_cfg.cfg.lpg.onTime = PWM_LGP_ONTIME_15_6MS;
	prvErrOutput(iot_pwm_set(&pwm_cfg));


	prvErrOutput(iot_pwm_open(OPENAT_PWM_PWL_OUT0));
	pwm_cfg.port = OPENAT_PWM_PWL_OUT0;
	pwm_cfg.cfg.pwl.freq = 0;
	pwm_cfg.cfg.pwl.level = 50;
	prvErrOutput(iot_pwm_set(&pwm_cfg));

	prvErrOutput(iot_pwm_open(OPENAT_PWM_PWL_OUT1));
	pwm_cfg.port = OPENAT_PWM_PWL_OUT1;
	pwm_cfg.cfg.pwl.freq = 0;
	pwm_cfg.cfg.pwl.level = 50;
	prvErrOutput(iot_pwm_set(&pwm_cfg));

	iot_os_sleep(5000);
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		prvTest();

		iot_os_sleep(2000);
	}
}

void pwmTest(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "pwm");
}
