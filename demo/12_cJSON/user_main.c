/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-06-15 00:10:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
//#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "am_openat.h"
#include "am_openat_vat.h"
#include "am_openat_common.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "iot_adc.h"
#include "iot_vat.h"
#include "iot_network.h"
#include "iot_socket.h"

#include "cJSON.h"
//#include "MQTTClient.h"

HANDLE TestTask_HANDLE = NULL;

#define jsonRoot "{\r\n"                                                                             \
				 "\"imei\": \"8661111111111111\",\r\n"                                               \
				 "\"Num\": 142,\r\n"                                                                 \
				 "\"Value\": {\r\n"                                                                  \
				 "\"name\": \"cx\",\r\n"                                                             \
				 "\"age\": 18,\r\n"                                                                  \
				 "\"blog\": \"https://blog.csdn.net/weixin_44570083/article/details/104285283\"\r\n" \
				 "},\r\n"                                                                            \
				 "\"hexArry\": [31, 56, 36, 1365, 263]\r\n"                                          \
				 "}\r\n"

//JSON解析
void cJSON_Parsing()
{
	// jsonRoot 是您要剖析的数据
	//首先整体判断是否为一个json格式的数据
	iot_debug_print("[cJSON_Test] cJSON_Parsing Start");
	cJSON *pJsonRoot = cJSON_Parse(jsonRoot);
	//如果是否json格式数据
	if (pJsonRoot != NULL)
	{
		iot_debug_print("[cJSON_Test] cJSON TRUE");
		iot_debug_print("[cJSON_Test] cJSON:%s", jsonRoot);
	}
	else
	{
		iot_debug_print("[cJSON_Test] cJSON ERROR");
	}

	//解析imei字段字符串内容
	cJSON *pimeiAdress = cJSON_GetObjectItem(pJsonRoot, "imei");
	//判断imei字段是否json格式
	if (pimeiAdress)
	{
		//判断mac字段是否string类型
		if (cJSON_IsString(pimeiAdress))
			iot_debug_print("[cJSON_Test] get imeiAdress:%s", pimeiAdress->valuestring);
	}
	else
		iot_debug_print("[cJSON_Test] get imeiAdress failed");

	//解析Num字段int内容
	cJSON *pNumber = cJSON_GetObjectItem(pJsonRoot, "Num");
	//判断Num字段是否存在
	if (pNumber)
	{
		//判断mac字段是否数字整型类型
		if (cJSON_IsNumber(pNumber))
			iot_debug_print("[cJSON_Test] get Num:%d", pNumber->valueint);
	}
	else
		iot_debug_print("[cJSON_Test] get Num failed");

	//解析value字段内容，判断是否为json
	cJSON *pValue = cJSON_GetObjectItem(pJsonRoot, "Value");
	if (pValue)
	{
		//进一步剖析里面的name字段:注意这个根节点是 pValue
		cJSON *pName = cJSON_GetObjectItem(pValue, "name");
		if (pName)
		{
			if (cJSON_IsString(pName))
				iot_debug_print("[cJSON_Test] get value->Name:%s", pName->valuestring);
		}
		else
			iot_debug_print("[cJSON_Test] get pValue->pName failed");

		//进一步剖析里面的age字段:注意这个根节点是 pValue
		cJSON *pAge = cJSON_GetObjectItem(pValue, "age");
		if (pAge)
		{
			if (cJSON_IsNumber(pAge))
				iot_debug_print("[cJSON_Test] get value->Age:%d", pAge->valueint);
		}
		else
			iot_debug_print("[cJSON_Test] get pValue->pAge failed");

		//进一步剖析里面的blog字段:注意这个根节点是 pValue
		cJSON *pBlog = cJSON_GetObjectItem(pValue, "blog");
		if (pBlog)
		{
			if (cJSON_IsString(pBlog))
				iot_debug_print("[cJSON_Test] get value->pBlog:%s", pBlog->valuestring);
		}
		else
			iot_debug_print("[cJSON_Test] get pValue->pBlog failed");
	}
	else
		iot_debug_print("[cJSON_Test] get pValue failed");

	//剖析数组
	cJSON *pArry = cJSON_GetObjectItem(pJsonRoot, "hexArry");
	if (pArry)
	{
		//获取数组长度
		int arryLength = cJSON_GetArraySize(pArry);
		iot_debug_print("[cJSON_Test] get arryLength:%d", arryLength);
		//逐个打印
		int i;
		for (i = 0; i < arryLength; i++)
			iot_debug_print("[cJSON_Test] get cJSON_GetArrayItem(pArry, %d)= %d", i, cJSON_GetArrayItem(pArry, i)->valueint);
	}
	else
		iot_debug_print("[cJSON_Test] get pArry failed");

	//释放内存
	cJSON_Delete(pJsonRoot);

	iot_debug_print("[cJSON_Test] cJSON_Parsing Stop");
}

//JSON生成
void cJSON_Generate()
{
	//取一下本地的station的mac地址，保存在全局变量tempMessage
	iot_debug_print("[cJSON_Test] cJSON_Generate Start");
	cJSON *pRoot = cJSON_CreateObject();

	//新增一个字段imei到根点，数值是tempMessage
	char tempMessage[] = "8661111111111111";
	cJSON_AddStringToObject(pRoot, "imei", tempMessage);

	//新增一个字段number到根点，数值是2
	cJSON_AddNumberToObject(pRoot, "number", 2020);

	cJSON *pValue = cJSON_CreateObject();
	cJSON_AddStringToObject(pValue, "name", "cx");
	cJSON_AddNumberToObject(pValue, "age", 17);
	cJSON_AddItemToObject(pRoot, "value", pValue);

	//数组初始化
	int hex[5] = {11, 12, 13, 14, 15};
	cJSON *pHex = cJSON_CreateIntArray(hex, 5); //创建一个长度为5的int型的数组json元素
	cJSON_AddItemToObject(pRoot, "hex", pHex);	//将数组元素添加进pRoot

	char *s = cJSON_Print(pRoot);
	iot_debug_print("[cJSON_Test] creatJson:%s", s);
	//释放内存
	cJSON_free((void *)s);

	//释放内存
	//cJSON_Delete(pHex);
	//释放内存
	//cJSON_Delete(pValue);
	//释放内存
	cJSON_Delete(pRoot);
	iot_debug_print("[cJSON_Test] cJSON_Generate Stop");
}

//main函数
int appimg_enter(void *param)
{
    //关闭看门狗，死机不会重启。默认打开
    iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
    //打开调试信息，默认关闭
    iot_vat_send_cmd("AT^TRACECTRL=0,1,3\r\n", sizeof("AT^TRACECTRL=0,1,3\r\n"));
    iot_debug_print("[hello]appimg_enter");
	cJSON_Parsing();
	cJSON_Generate();
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
