#include "driver_comm.h"


#undef DRV_NAME
#define DRV_NAME "DRV_I2C"
#define I2C_SALVE_ADDR (0X48)
static HANDLE testTask;



static void prvTest(void)
{
	T_AMOPENAT_I2C_PARAM param;
	UINT32 len, chan;
	float temp;
	UINT8 pRegAddr = 0;
	UINT8 buf[10];
	memset(&param, 0, sizeof(param));
	param.freq = 100000;
	param.regAddrBytes = I2C_SALVE_ADDR;

	for (chan=OPENAT_I2C_2; chan<=OPENAT_I2C_3; chan++)
	{
		DRV_PRINT_RESULT(iot_i2c_open(chan, &param));

		memset(buf, 0, sizeof(buf));
		
		DRV_PRINT_RESULT((len = iot_i2c_read(chan, I2C_SALVE_ADDR, &pRegAddr, buf, 2)));

		/*高11位标识温度，每个只为0.125读*/
		temp = ((buf[0] << 8 | buf[1]) >> 5) * 0.125;

		DRV_PRINT("[%s,%d] len %d, chan %d,temp %f°C", __FUNCTION__, __LINE__, len, chan, temp);

		DRV_PRINT_RESULT(iot_i2c_close(chan));
	}
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		prvTest();

		iot_os_sleep(5000);
		
	}
}

void pub_enable_i2c(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "i2c");
}

