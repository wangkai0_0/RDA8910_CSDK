/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-05-26 21:08:07
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "am_openat.h"
#include "am_openat_vat.h"
#include "am_openat_common.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "iot_adc.h"
#include "iot_vat.h"
#include "iot_network.h"
#include "iot_socket.h"

//Udp Demo
//UDP模式指定IP和端口就可以的
//UDP服务没有客户端和服务器的概念，UDP是对等网络。tcp服务才有服务器和客户端的概念。
//错的人多了就变成对的了

//远端ip和port
#define UDP_REMOTE_IP "121.40.170.41"
#define UDP_REMOTE_PORT 12414

HANDLE TestTask_HANDLE = NULL;
uint8 NetWorkCbMessage = 0;
int socketfd = -1;

static void SentTask(void *param)
{

	struct openat_sockaddr_in udp_remote_addr = {0};
	udp_remote_addr.sin_family = OPENAT_AF_INET;
	udp_remote_addr.sin_port = htons((unsigned short)UDP_REMOTE_PORT);
	inet_aton(UDP_REMOTE_IP, &udp_remote_addr.sin_addr);

	uint8 num = 0;
	int len = 0;
	char data[512] = {0};
	while (1)
	{
		if (socketfd >= 0)
		{
			len = sprintf(data, "RDA8910 Sent:%d", num);
			data[len] = '\0';
			iot_debug_print(data);
			if (len > 0)
			{
				// UDP 发送数据
				len = sendto(socketfd, data, len + 1, 0, (const struct openat_sockaddr *)&udp_remote_addr, sizeof(struct openat_sockaddr));
				if (len < 0)
				{
					iot_debug_print("[socket] udp send data False");
				}
				else
				{
					iot_debug_print("[socket] udp send data Len = %d", len);
					num += 1;
				}
			}
		}
		iot_os_sleep(3000);
	}
}

static void RecvTask(void *param)
{
	struct openat_sockaddr_in udp_remote_addr = {0};
	openat_socklen_t udp_remote_len = 0;
	int len = 0;
	unsigned char data[512] = {0};
	while (1)
	{
		if (socketfd >= 0)
		{
			// UDP 接受数据
			len = recvfrom(socketfd, data, sizeof(data), 0, (struct openat_sockaddr *)&udp_remote_addr, &udp_remote_len);
			if (len < 0)
			{
				iot_debug_print("[socket] udp Recv data False");
			}
			else
			{
				//发现回去不到远端ip和端口
				iot_debug_print("[socket] udp Recv from ip:%s,port:%d", inet_ntoa(udp_remote_addr.sin_addr),ntohs(udp_remote_addr.sin_port));
				iot_debug_print("[socket] udp Recv data result = %s", data);
			}
		}
	}
}
static void UdpInit()
{
	//创建套接字,Udp连接
	socketfd = socket(OPENAT_AF_INET, OPENAT_SOCK_DGRAM, 0);
	while (socketfd < 0)
	{
		iot_debug_print("[socket] create udp socket error");
		iot_os_sleep(3000);
	}
	iot_debug_print("[socket] udp connect success");
	iot_os_create_task(SentTask, NULL, 2048, 10, OPENAT_OS_CREATE_DEFAULT, "SentTask");
	iot_os_create_task(RecvTask, NULL, 2048, 10, OPENAT_OS_CREATE_DEFAULT, "RecvTask");
}
static void TestTask(void *param)
{
	bool NetLink = FALSE;
	while (NetLink == FALSE)
	{
		T_OPENAT_NETWORK_CONNECT networkparam = {0};
		switch (NetWorkCbMessage)
		{
		case OPENAT_NETWORK_DISCONNECT: //网络断开 表示GPRS网络不可用澹，无法进行数据连接，有可能可以打电话
			iot_debug_print("[socket] OPENAT_NETWORK_DISCONNECT");
			iot_os_sleep(1000);
			break;
		case OPENAT_NETWORK_READY: //网络已连接 表示GPRS网络可用，可以进行链路激活
			iot_debug_print("[socket] OPENAT_NETWORK_READY");
			memcpy(networkparam.apn, "CMNET", strlen("CMNET"));
			//建立网络连接，实际为pdp激活流程
			iot_network_connect(&networkparam);
			iot_os_sleep(500);
			break;
		case OPENAT_NETWORK_LINKED: //链路已经激活 PDP已经激活，可以通过socket接口建立数据连接
			iot_debug_print("[socket] OPENAT_NETWORK_LINKED");
			NetLink = TRUE;
			break;
		}
	}
	if (NetLink == TRUE)
	{
		UdpInit();
	}
	iot_os_delete_task(TestTask_HANDLE);
}
static void NetWorkCb(E_OPENAT_NETWORK_STATE state)
{
	NetWorkCbMessage = state;
}
//main函数
int appimg_enter(void *param)
{
	//注册网络状态回调函数
	iot_network_set_cb(NetWorkCb);
    //关闭看门狗，死机不会重启。默认打开
    iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
    //打开调试信息，默认关闭
    iot_vat_send_cmd("AT^TRACECTRL=0,1,3\r\n", sizeof("AT^TRACECTRL=0,1,3\r\n"));
    iot_debug_print("[hello]appimg_enter");
	//创建一个任务
	//TestTask_HANDLE =
	TestTask_HANDLE = iot_os_create_task(TestTask, NULL, 2048, 10, OPENAT_OS_CREATE_DEFAULT, "TestTask");
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
