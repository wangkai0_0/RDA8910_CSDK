#include "driver_comm.h"
#include "camera.h"

#undef DRV_NAME
#define DRV_NAME "DRV_CAMERA"

static HANDLE testTask;

unsigned char *gScannerBuff = NULL;

/*提取yuv数据中的y*/
static unsigned char * zbar_scannerY(unsigned char *data)
{
	unsigned char *src = data,*end = data + (CAM_SENSOR_WIDTH * CAM_SENSOR_HEIGHT *2);
	unsigned char *dst = gScannerBuff;
	while (src < end)
	{
		src ++;
		*dst = *src;
		src ++;
		dst++;
	}//End of while;

    return gScannerBuff;
}

/*解析二维码中的数据*/
static void zbar_scannerRun(int width, int height, int size, unsigned char *dataInput)
{
	int len;
	char *data;
	
	//创建句柄， handle != 0 表示解码成功
	int handle = iot_zbar_scannerOpen(width, height, size, dataInput);
    
	if (handle)
	{
		do
		{        
			// 解码成功获取二维码信息
			data = iot_zbar_getData(handle, &len);
			data[len] = 0;

			iot_debug_print("[zbar] zbar_scanner_run come in handle_data %s", data);

		 // 判断是否有下一个数据
		}while(iot_zbar_findNextData(handle) > 0);

		// 释放句柄
		iot_zbar_scannerClose(handle);
	}
}

/*预览回调函数, 获取预览数据*/
void cameraCallback(T_AMOPENAT_CAMERA_MESSAGE *pMsg)
{

	iot_debug_print("[zbar] camera_evevt_callback");
    switch(pMsg->evtId)
    {
        case OPENAT_DRV_EVT_CAMERA_DATA_IND:
        {
            // 获取camera得到的数据， 发送到zbartask 去解析
            zbar_scannerRun(CAM_SENSOR_WIDTH, CAM_SENSOR_HEIGHT,CAM_SENSOR_HEIGHT*CAM_SENSOR_WIDTH, zbar_scannerY((unsigned char *)pMsg->dataParam.data));
            break;
        }
        default:
            break;
    }
}

BOOL cameraInit(void)
{
	BOOL ret;
	T_AMOPENAT_CAM_PREVIEW_PARAM previewParam;
	T_AMOPENAT_CAMERA_PARAM initParam =
	{
		cameraCallback,
		OPENAT_I2C_1, 
		CAM_SPI_SLAVE_ADDR,
		AMOPENAT_CAMERA_REG_ADDR_8BITS|AMOPENAT_CAMERA_REG_DATA_8BITS,

		TRUE,
		TRUE,
		TRUE, 

		CAM_SENSOR_WIDTH,
		CAM_SENSOR_HEIGHT,
		CAMERA_IMAGE_FORMAT_YUV422,
		cameraInitReg,
        sizeof(cameraInitReg)/sizeof(AMOPENAT_CAMERA_REG),
		{CAM_ID_REG, CAM_ID_VALUE},
		{2,3,TRUE},
		1,
		OPENAT_SPI_MODE_MASTER2_2,
		OPENAT_SPI_OUT_Y1_V0_Y0_U0,
		FALSE,
		CAM_SPI_SPEED
	};
	ret = iot_camera_init(&initParam);
	if (!ret)
		return FALSE;
	
	ret = iot_camera_poweron(FALSE);
	if (!ret)
		return FALSE;

	previewParam.startX = 0;
	previewParam.startY = 0;
	
	/*++/测试写lcd放缩显示和反转显示功能，需要可以打开宏*/
	/*放缩显示4倍*/
	previewParam.zoom = -2;
	/* 反转90度*/
	previewParam.rotation = 90;

	/*显示的区域也需要按照2倍放缩，反转90读，切换宽高*/
	previewParam.endX = LCD_WIDTH;
	previewParam.endY = LCD_HEIGHT;

	/*--/测试写lcd放缩显示和反转显示功能，需要可以打开宏*/
	
	ret = iot_camera_preview_open(&previewParam);
	if (!ret)
		return FALSE;

	iot_debug_print("zwb camera_writeReg test");

	return TRUE;
}


static void prvTest(void)
{
	gScannerBuff = iot_os_malloc(CAM_SENSOR_HEIGHT*CAM_SENSOR_WIDTH);

	cameraInit();
}

static void PrvTestMain(void *pParameter)
{
	int count = 0;

	prvTest();
	
	while(1)
	{
		DRV_PRINT("[%s,%d] count %d", __FUNCTION__, __LINE__, count++);

		iot_os_sleep(5000);
	}
}

void cmaeraTest(void)
{
    testTask = iot_os_create_task(PrvTestMain, NULL, 
        DRV_TASK_STACK_SIZE, DRV_TASK_PRIO, OPENAT_OS_CREATE_DEFAULT, "camera");
}

