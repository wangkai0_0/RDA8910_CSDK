/***************
	demo_hello
****************/
#include <string.h>
#include "iot_debug.h"
#include "iot_os.h"
#include "iot_lcd.h"
#include "iot_pmd.h"
#include "am_openat_drv.h"
#include "lcd.h"
#include "res_touch.h"

#define LCD_WIDTH 240
#define LCD_HEIGH 320
#define LCD_PIXEL_BYTES 2 //两字节16色

const UINT32 lcdRegTable[] =
    {
        0x00010090,
        0x11,
        0x00010090,
        0xCF,
        0x00030000,
        0x00030099,
        0x00030030,
        0xED,
        0x00030064,
        0x00030003,
        0x00030012,
        0x00030081,
        0xCB,
        0x00030039,
        0x0003002C,
        0x00030000,
        0x00030034,
        0x00030002,
        0xEA,
        0x00030000,
        0x00030000,
        0xE8,
        0x00030085,
        0x00030000,
        0x00030078,
        0xC0,
        0x00030023,
        0xC1,
        0x00030012,
        0xC2,
        0x00030011,
        0xC5,
        0x00030040,
        0x00030030,
        0xC7,
        0x000300A9,
        0x3A,
        0x00030055,
        0x36,
        0x00030008,
        0xB1,
        0x00030000,
        0x00030018,
        0xB6,
        0x0003000A,
        0x000300A2,
        0xF2,
        0x00030000,
        0xF7,
        0x00030020,
        0x26,
        0x00030001,
        0xE0,
        0x0003001F,
        0x00030024,
        0x00030023,
        0x0003000B,
        0x0003000F,
        0x00030008,
        0x00030050,
        0x000300D8,
        0x0003003B,
        0x00030008,
        0x0003000A,
        0x00030000,
        0x00030000,
        0x00030000,
        0x00030000,
        0xE1,
        0x00030000,
        0x0003001B,
        0x0003001C,
        0x00030004,
        0x00030010,
        0x00030007,
        0x0003002F,
        0x00030027,
        0x00030044,
        0x00030007,
        0x00030015,
        0x0003000F,
        0x0003003F,
        0x0003003F,
        0x0003001F,
        0x29,
};

static void write_command_table(const UINT32 *table, UINT16 size)
{
    UINT16 flag;
    UINT16 value;
    UINT16 index;

    for (index = 0; index < size && table[index] != (UINT32)-1; index++)
    {
        flag = table[index] >> 16;
        value = table[index] & 0xffff;

        switch (flag)
        {
        case 1:
            iot_os_sleep(value);
            break;

        case 0:
        case 2:
            iot_lcd_write_cmd(value & 0x00ff);
            break;

        case 3:
            iot_lcd_write_data(value & 0x00ff);
            break;

        default:
            break;
        }
    }
}

BOOL lcdInit(void)
{
    iot_pmd_poweron_ldo(OPENAT_LDO_POWER_VLCD, 15);
    T_AMOPENAT_COLOR_LCD_PARAM param = {0};
    param.width = LCD_WIDTH;
    param.height = LCD_HEIGH;
    param.msgCallback = NULL;
    param.bus = OPENAT_LCD_SPI4LINE;
    param.lcdItf.spi.frequence = 50000000;
    iot_lcd_color_init(&param);
    write_command_table(lcdRegTable, sizeof(lcdRegTable) / sizeof(int));
    return TRUE;
}

BOOL lcdScreen(T_AMOPENAT_LCD_RECT_T *rect, UINT16 *pDisplayBuffer)
{
    iot_lcd_update_color_screen(rect, pDisplayBuffer);
}

void app_lcd_test()
{

    while (1)
    {
        LCD_Clear(); //清屏
        LCD_SetPointColor(PIXEL_2BYTES_BLACK);
        LCD_ShowChar(10, 0, '1', 24, 1);
        LCD_SetPointColor(PIXEL_2BYTES_BLUE);
        LCD_ShowString(10, 30, "hello", 24);
        LCD_SetPointColor(PIXEL_2BYTES_RED);
        LCD_ShowNum(10, 60, 123, 3, 24);

        LCD_SetPointColor(PIXEL_2BYTES_YELLOW);
        T_AMOPENAT_LCD_RECT_T rect = {0};
        rect.ltX = 50;
        rect.ltY = 150;
        rect.rbX = rect.ltX + 50;
        rect.rbY = rect.ltY + 50;
        LCD_Fill(&rect, 1);

        LCD_Refresh_Gram();
        iot_os_sleep(1000);
    }
}


static void res_touch_test2(PVOID pParameter)
{
    LCD_Clear(); //清屏
    LCD_Refresh_Gram();
    while (1)
    {
        LCD_Refresh_Gram();
        iot_os_sleep(100);
    }
}

static void res_touch_test(PVOID pParameter)
{
    LCD_Clear(); //清屏
    LCD_Refresh_Gram();
    Touch_init();
    if (TP_Read_Adjdata() == FALSE)
    {
        while (Touch_Adjust() == FALSE)
            iot_os_sleep(1000);
    }

    iot_os_create_task(res_touch_test2, NULL, 2048, 1, OPENAT_OS_CREATE_DEFAULT, "res_touch_test");
    while (1)
    {
        u16 x, y;
        if(!TP_Read_Screen_XY(&x, &y))
            continue;

        LCD_Draw_Big_Point(x, y);
        iot_os_sleep(10);
    }
}


int appimg_enter(void *param)
{
    iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
    //打开调试信息，默认关闭
    iot_vat_send_cmd((UINT8 *)"AT^TRACECTRL=0,1,1\r\n", sizeof("AT^TRACECTRL=0,1,1\r\n"));

    iot_debug_print("[hello]appimg_enter");
    APP_LCD_DATA lcd_data;
    lcd_data.lcd_width = LCD_WIDTH;
    lcd_data.lcd_heigh = LCD_HEIGH;
    lcd_data.lcd_pixel_bytes = LCD_PIXEL_BYTES;
    lcd_data.lcd_point_color = PIXEL_2BYTES_BLACK;
    lcd_data.lcd_back_color = PIXEL_2BYTES_WHITE;
    lcd_data.lcd_init = lcdInit;
    lcd_data.lcd_screen = lcdScreen;
    LCD_Setup(&lcd_data);

    //iot_os_create_task(app_lcd_test, NULL, 1024, 1, OPENAT_OS_CREATE_DEFAULT, "app_lcd_test");

    iot_os_create_task(res_touch_test, NULL, 2048, 1, OPENAT_OS_CREATE_DEFAULT, "res_touch_test");
    
    return 0;
}

void appimg_exit(void)
{
    iot_debug_print("[hello]appimg_exit");
}
