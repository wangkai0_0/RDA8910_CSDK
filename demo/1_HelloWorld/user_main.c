/*
 * @Author: your name
 * @Date: 2020-05-14 12:49:43
 * @LastEditTime: 2020-05-19 13:28:59
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \csdk\USER\user_main.c
 */

#include "osi_log.h"
#include "osi_api.h"

#include "string.h"
#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
// 0_HelloWorld
static void TestTask(void *param)
{
	while (1)
	{
		OSI_LOGI(0, "OSI_LOGI hello world");
		iot_debug_print("iot_debug_print hello world");
		//休眠500ms
		osiThreadSleep(500);
	}
	osiThreadExit();
}

//main函数
int appimg_enter(void *param)
{
    //关闭看门狗，死机不会重启。默认打开
    iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
    //打开调试信息，默认关闭
    iot_vat_send_cmd("AT^TRACECTRL=0,1,3\r\n", sizeof("AT^TRACECTRL=0,1,3\r\n"));
    iot_debug_print("[hello]appimg_enter");
	//创建一个任务
	osiThreadCreate("TestTask", TestTask, NULL, OSI_PRIORITY_NORMAL, 1024, 0);
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
