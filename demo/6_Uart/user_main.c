/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-05-22 19:35:50
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "am_openat.h"
#include "am_openat_vat.h"
#include "am_openat_common.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "iot_adc.h"
#include "iot_vat.h"

void uart1_recv_handle(T_AMOPENAT_UART_MESSAGE *evt)
{
	uint8 *recv_buff = NULL;
	uint8 dataLen = evt->param.dataLen;
	if (dataLen)
	{
		if (evt->evtId = OPENAT_DRV_EVT_UART_RX_DATA_IND)
		{
			recv_buff = iot_os_malloc(dataLen);
			if (recv_buff == NULL)
			{
				iot_debug_print("uart1_recv_handle_0 recv_buff malloc fail %d", dataLen);
			}
			uint8 len = iot_uart_read(OPENAT_UART_1, recv_buff, dataLen, 25);
			recv_buff[len] = '\0';
			if (len)
			{
				char buf[100] = {0};
				sprintf(buf, "uart1 recv dadalen:%d,data:%s\r\n", len, recv_buff);
				iot_debug_print(buf);
				iot_uart_write(OPENAT_UART_1, buf, strlen(buf));
			}
			//使用完必须释放，不然会造成堆区内存泄漏，久而久之内存就不够用了。
			iot_os_free(recv_buff);
		}
	}
}
void uart2_recv_handle(T_AMOPENAT_UART_MESSAGE *evt)
{
	uint8 *recv_buff = NULL;
	uint8 dataLen = evt->param.dataLen;
	if (dataLen)
	{
		if (evt->evtId = OPENAT_DRV_EVT_UART_RX_DATA_IND)
		{
			recv_buff = iot_os_malloc(dataLen);
			if (recv_buff == NULL)
			{
				iot_debug_print("uart1_recv_handle_0 recv_buff malloc fail %d", dataLen);
			}
			uint8 len = iot_uart_read(OPENAT_UART_2, recv_buff, dataLen, 25);
			recv_buff[len] = '\0';
			if (len)
			{
				char buf[100] = {0};
				sprintf(buf, "uart2 recv dadalen:%d,data:%s\r\n", len, recv_buff);
				iot_debug_print(buf);
				iot_uart_write(OPENAT_UART_2, buf, strlen(buf));
			}
			//使用完必须释放，不然会造成堆区内存泄漏，久而久之内存就不够用了。
			iot_os_free(recv_buff);
		}
	}
}

//main函数
int appimg_enter(void *param)
{
    //关闭看门狗，死机不会重启。默认打开
    iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
    //打开调试信息，默认关闭
    iot_vat_send_cmd("AT^TRACECTRL=0,1,3\r\n", sizeof("AT^TRACECTRL=0,1,3\r\n"));
    iot_debug_print("[hello]appimg_enter");

	T_AMOPENAT_UART_PARAM uartCfg = {0};
	BOOL err = 0;
	uartCfg.baud = OPENAT_UART_BAUD_115200;				//波特率
	uartCfg.dataBits = 8;								//数据位
	uartCfg.stopBits = 1;								// 停止位
	uartCfg.parity = OPENAT_UART_NO_PARITY;				// 无校验
	uartCfg.flowControl = OPENAT_UART_FLOWCONTROL_NONE; //无流控
	uartCfg.txDoneReport = FALSE;						//发送完成不通知
	uartCfg.uartMsgHande = uart1_recv_handle;			//回调函数
	err = iot_uart_open(OPENAT_UART_1, &uartCfg);		//初始化串口1
	while (!err)
	{
		iot_debug_print("OPENAT_UART_1 open FALSE");
		iot_os_sleep(1000);
	}
	uartCfg.uartMsgHande = uart2_recv_handle;	  //回调函数
	err = iot_uart_open(OPENAT_UART_2, &uartCfg); //初始化串口2
	while (!err)
	{
		iot_debug_print("OPENAT_UART_2 open FALSE");
		iot_os_sleep(1000);
	}

	//创建一个任务
	//iot_os_create_task(TestTask, NULL, 4096, 1, OPENAT_OS_CREATE_DEFAULT, "TestTask");
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
