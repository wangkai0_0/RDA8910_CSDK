/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-05-20 15:38:37
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "iot_adc.h"

static void ADC2(void *param)
{
	BOOL err = iot_adc_init(OPENAT_ADC_2, OPENAT_ADC_MODE_MAX);
	while (!err)
	{
		iot_debug_print("ADC2 Init FALSE");
		osiThreadSleep(1000);
	}
	UINT32 adcValue = 0, voltage = 0;
	while (1)
	{
		err = iot_adc_read(OPENAT_ADC_2, &adcValue, &voltage);
		while (!err)
		{
			iot_debug_print("ADC2 read FALSE");
			osiThreadSleep(1000);
		}
		iot_debug_print("ADC2 read adcValue:%d,voltage:%d", adcValue, voltage);
		//线程休眠500ms
		osiThreadSleep(1000);
	}
}

static void ADC3(void *param)
{
	BOOL err = iot_adc_init(OPENAT_ADC_3, OPENAT_ADC_MODE_MAX);
	while (!err)
	{
		iot_debug_print("ADC3 Init FALSE");
		osiThreadSleep(1000);
	}
	UINT32 adcValue = 0, voltage = 0;
	while (1)
	{
		err = iot_adc_read(OPENAT_ADC_3, &adcValue, &voltage);
		while (!err)
		{
			iot_debug_print("ADC3 read FALSE");
			osiThreadSleep(1000);
		}
		iot_debug_print("ADC3 read adcValue:%d,voltage:%d", adcValue, voltage);
		//线程休眠500ms
		osiThreadSleep(1000);
	}
	osiThreadExit();
}

//main函数
int appimg_enter(void *param)
{
    //关闭看门狗，死机不会重启。默认打开
    iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
    //打开调试信息，默认关闭
    iot_vat_send_cmd("AT^TRACECTRL=0,1,3\r\n", sizeof("AT^TRACECTRL=0,1,3\r\n"));
    iot_debug_print("[hello]appimg_enter");

	osiThreadCreate("ADC2", ADC2, NULL, OSI_PRIORITY_NORMAL, 2048, 0);
	osiThreadCreate("ADC3", ADC3, NULL, OSI_PRIORITY_NORMAL, 2048, 0);
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
