#include "ds_common.h"

E_OPENAT_NETWORK_STATE g_network_state = OPENAT_NETWORK_DISCONNECT;
#define NET_LED_GPIO 4


static VOID network_ind_cb(E_OPENAT_NETWORK_STATE state)
{
	g_network_state = state;
}


static void netled_task(void *param)
{
	unsigned char net_state = 0;
	while(1)
	{
		if(g_network_state == OPENAT_NETWORK_LINKED)
		{
			iot_os_sleep(100);	
		}
		else
		{
			iot_os_sleep(1000);
			ds_error("[netled_task] net_state = %d",net_state);
		}
		net_state ^= 1;
		
		iot_gpio_set(NET_LED_GPIO, net_state);
	}
}

void network_ind_init()
{
	T_AMOPENAT_GPIO_CFG  output_cfg;
    memset(&output_cfg, 0, sizeof(T_AMOPENAT_GPIO_CFG));
    
    output_cfg.mode = OPENAT_GPIO_OUTPUT; //�������
    output_cfg.param.defaultState = FALSE; // Ĭ�ϵ͵�ƽ

    iot_gpio_open(NET_LED_GPIO, &output_cfg);
	iot_pmd_poweron_ldo(OPENAT_LDO_POWER_VLCD,6);
	
	iot_os_create_task(netled_task,
                      NULL,
                      2048,
                      5,
                      OPENAT_OS_CREATE_DEFAULT,
                      "netled_task");
	iot_network_set_cb(network_ind_cb);	
}



