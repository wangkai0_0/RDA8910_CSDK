#ifndef __APP_LCD_H
#define __APP_LCD_H

#include "cs_types.h"
#include "string.h"
#include "am_openat_drv.h"
#include "iot_types.h"

typedef struct _APP_LCD_DATA
{
    int lcd_width;                                                           //LCD宽
    int lcd_heigh;                                                           //LCD高
    int lcd_pixel_bytes;                                                     //LCD一个像素占用的字节
    int lcd_point_color;                                                     //画笔颜色
    int lcd_back_color;                                                      //背景颜色
    void (*lcd_init)(void);                                                  //初始化函数
    void (*lcd_screen)(T_AMOPENAT_LCD_RECT_T *rect, UINT16 *pDisplayBuffer); //刷屏函数
} APP_LCD_DATA;

typedef enum
{
    PIXEL_2BYTES_WHITE = 0xFFFF,   //白色
    PIXEL_2BYTES_BLACK = 0x0000,   //黑色
    PIXEL_2BYTES_BLUE = 0x001F,    //蓝色
    PIXEL_2BYTES_BRED = 0XF81F,    //黑红
    PIXEL_2BYTES_GRED = 0XFFE0,    //绿红
    PIXEL_2BYTES_GBLUE = 0X07FF,   //蓝绿
    PIXEL_2BYTES_RED = 0xF800,     //红色
    PIXEL_2BYTES_MAGENTA = 0xF81F, //洋红
    PIXEL_2BYTES_GREEN = 0x07E0,   //绿色
    PIXEL_2BYTES_CYAN = 0x7FFF,    //青色
    PIXEL_2BYTES_YELLOW = 0xFFE0,  //黄色
    PIXEL_2BYTES_BROWN = 0XBC40,   //棕色
    PIXEL_2BYTES_BRRED = 0XFC07,   //棕红
    PIXEL_2BYTES_GRAY = 0X8430     //灰色
} APP_LCD_PIXEL_2BYTES_COLOR;



APP_LCD_DATA *LCD_GET_SYS_DATA(void);

//设置画笔颜色
void LCD_SetPointColor(APP_LCD_PIXEL_2BYTES_COLOR color);

//设置背景颜色
void LCD_SetBackColor(APP_LCD_PIXEL_2BYTES_COLOR color);

//画点
//x:0~240
//y:0~320
//t:1 填充 0,清空
void LCD_DrawPoint(int x, int y, uint8_t t);

//画一个大点(2*2的点)
//x,y:坐标
//color:颜色
void LCD_Draw_Big_Point(u16 x, u16 y);

//在指定区域内填充颜色
void LCD_Fill(T_AMOPENAT_LCD_RECT_T *rect, uint8_t mode);

//清屏函数,按照背景颜色清屏
void LCD_Clear(void);

//在指定位置显示一个字符,包括部分字符
//x:0~127
//y:0~63
//size:选择字体 12/16/24
//mode:0,反白显示;1,正常显示
void LCD_ShowChar(int x, int y, uint8_t chr, uint8_t size, uint8_t mode);

//显示字符串
//x,y:起点坐标
//*p:字符串起始地址
//size:字体大小
void LCD_ShowString(int x, int y, const uint8_t *p, uint8_t size);

//显示数字
//x,y :起点坐标
//num:数值(0~4294967295);
//len :数字的位数
//size:字体大小
void LCD_ShowNum(int x, int y, uint32_t num, uint8_t len, uint8_t size);

//两点间画线功能
//x1：直线的起始x坐标
//y1：直线的起始y坐标
//x2：直线的结束x坐标
//y2：直线的结束y坐标
void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2);

//画矩形
//(x1,y1),(x2,y2):矩形的对角坐标
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2);

//在指定位置画一个指定大小的圆
//(x,y):中心点
//r    :半径
void LCD_Draw_Circle(u16 x0, u16 y0, u8 r);

//刷屏调用这个函数
void LCD_Refresh_Gram(void);

//设置一些必要的函数以及参数
void LCD_Setup(APP_LCD_DATA *lcd_data);
#endif
