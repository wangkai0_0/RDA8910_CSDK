@echo off
set CSDK_VER=1.0.0
set CSDK_PRO=10_HTTP_PostAndGet
set FOTA_FLAG=%1
set FOTA_TYPE=%2
set AM_MODEL=iot_SDK_720U
call ..\tools\core_launch.bat 10_HTTP_PostAndGet

REM/*+\NEW\chenzhimin\2020.07.22\*/
cd %PROJECT_OUT%
cmake ..\.. -G Ninja ^
-D CONFIG_BUILD_APP_HTTPCLIENT=ON
ninja
cd ..\..\project
REM/*-\NEW\chenzhimin\2020.07.22\*/