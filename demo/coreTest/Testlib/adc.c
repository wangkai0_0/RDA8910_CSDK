

#if 0
#include "iot_adc.h"


static HANDLE AdcTaskHandle = NULL;
static E_AMOPENAT_ADC_CHANNEL AdcTestChannel[] = {OPENAT_ADC_1, OPENAT_ADC_2};

static VOID adcTask(PVOID pParameter)
{
    UINT32 adcValue = 0, voltage = 0;
    while (1)
    {
        for (char i = 0; i < sizeof(AdcTaskHandle) / sizeof(E_AMOPENAT_ADC_CHANNEL); i++)
        {
            E_AMOPENAT_ADC_CHANNEL channel = AdcTestChannel[i];
            if (!iot_adc_init(channel, OPENAT_ADC_MODE_MAX))
            {
                iot_debug_print("[coreTest-False-adc] : ADC%d Init FALSE", channel);
                break;
            }
            if (!iot_adc_read(channel, &adcValue, &voltage))
            {
                iot_debug_print("[coreTest-False-adc] : ADC%d read FALSE", channel);
                break;
            }
            iot_debug_print("[coreTest-adc] : ADC%d read adcValue:%d,voltage:%d", channel, adcValue, voltage);
            iot_os_sleep(1000);
        }
    }
}

void pub_enable_adc(void)
{
    if (AdcTaskHandle == NULL)
        AdcTaskHandle = iot_os_create_task(adcTask, NULL, 1024, 1, OPENAT_OS_CREATE_DEFAULT, "adcTask");
}

void pub_disable_adc(void)
{
    if (AdcTaskHandle != NULL)
        iot_os_delete_task(AdcTaskHandle);
    AdcTaskHandle = NULL;
}

#endif
