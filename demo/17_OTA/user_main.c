/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-05-31 18:58:02
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */

#include "string.h"
#include "cs_types.h"

#include "osi_log.h"
#include "osi_api.h"

#include "am_openat.h"
#include "am_openat_vat.h"
#include "am_openat_common.h"

#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"
#include "iot_pmd.h"
#include "iot_adc.h"
#include "iot_vat.h"
#include "iot_network.h"
#include "iot_socket.h"
#include "httpclient.h"
#include "am_openat_httpclient.h"

HANDLE TestTask_HANDLE = NULL;
uint8 NetWorkCbMessage = 0;

#define FOTA_URL_FOR "http://iot.openluat.com/api/site/firmware_upgrade?project_key=%s&imei=%s&firmware_name=%s_CSDK_RDA8910&core_version=%s&dfota=1&version=%s"
#define PRODUCT_KEY "O5wpMkU7KdwSGIbQ6XrufIjr9GutlMyt"
#define IMEI "866714044529146"
#define CORE_VER "1031"

bool OTA_Start(void)
{

    if (iot_fota_init() != 0) //fail
    {
        iot_debug_print("[17_ota] fota_init fail");
        return false;
    }

    HTTP_SESSION_HANDLE pHTTP = HTTPClientOpenRequest(0);

    if (HTTPClientSetVerb(pHTTP, VerbGet) != HTTP_CLIENT_SUCCESS)
    {
        iot_debug_print("[17_ota] HTTPClientSetVerb error");
        return false;
    }

    if (HTTPClientAddRequestHeaders(pHTTP, "Accept", "*/*", TRUE) != HTTP_CLIENT_SUCCESS)
        return false;
    if (HTTPClientAddRequestHeaders(pHTTP, "Accept-Language", "cn", TRUE) != HTTP_CLIENT_SUCCESS)
        return false;
    if (HTTPClientAddRequestHeaders(pHTTP, "User-Agent", "*Mozilla/4.0", TRUE) != HTTP_CLIENT_SUCCESS)
        return false;
    if (HTTPClientAddRequestHeaders(pHTTP, "Connection", "Keep-Alive", TRUE) != HTTP_CLIENT_SUCCESS)
        return false;

    {
        char url[256] = {0};
        sprintf(url, FOTA_URL_FOR, PRODUCT_KEY, IMEI, CSDK_PRO, CORE_VER, CSDK_VER);
        if (HTTPClientSendRequest(pHTTP, url, NULL, 0, TRUE, 0, 0) != HTTP_CLIENT_SUCCESS)
        {
            iot_debug_print("[17_ota] HTTPClientSendRequest error");
            return false;
        }
    }

    if (HTTPClientRecvResponse(pHTTP, 20000) != HTTP_CLIENT_SUCCESS)
    {
        iot_debug_print("[17_ota] HTTPClientRecvResponse error");
        return false;
    }

    CHAR token[32] = {0};
    UINT32 tokenSize = 32;
    if (HTTPClientFindFirstHeader(pHTTP, "content-length", token, &tokenSize) != HTTP_CLIENT_SUCCESS)
    {
        iot_debug_print("[17_ota] HTTPClientFindFirstHeader error");
        return false;
    }
    else
    {
        iot_debug_print("[17_ota] HTTPClientFindFirstHeader %d,%s", tokenSize, token);
    }
    int fsz = 0; //固件包总大小
    if (strlen(token) > 0)
    {
        sscanf(token, "%*s %d", &fsz);
        iot_debug_print("[17_ota]GetSize fsz: %d", fsz);
    }
    else
    {
        iot_debug_print("[17_ota]GetSize faild");
        return false;
    }
    HTTPClientFindCloseHeader(pHTTP);
    UINT32 nRetCode;
    uint32 readTotalLen = 0; //总读取到字节数
    do
    {
        char readBuff[512] = {0};
        uint32 readSize = 0; //本次读取到字节数
        nRetCode = HTTPClientReadData(pHTTP, readBuff, sizeof(readBuff), 300, &readSize);
        //升级错误码
        if (!strncmp("{\"code\":", readBuff, sizeof("{\"code\":") - 1))
        {
            iot_debug_print("[17_ota] Error readBuff:%s", readBuff);
            return false;
        }
        readTotalLen += readSize;
        //远程升级
        if (iot_fota_download(readBuff, readSize, fsz) != 0)
        {
            iot_debug_print("[17_ota] iot_fota_download error");
            return false;
        }
    } while (nRetCode == HTTP_CLIENT_SUCCESS || nRetCode != HTTP_CLIENT_EOS);

    if (HTTPClientCloseRequest(&pHTTP) != HTTP_CLIENT_SUCCESS)
    {
        iot_debug_print("[17_ota] HTTPIntrnConnectionClose error");
        return false;
    }

    if (iot_fota_done() < 0)
    {
        iot_debug_print("[17_ota]fota error");
        return false;
    }

    return true;
}

static void TestTask(void *param)
{
    bool NetLink = FALSE;
    while (NetLink == FALSE)
    {
        T_OPENAT_NETWORK_CONNECT networkparam = {0};
        switch (NetWorkCbMessage)
        {
        case OPENAT_NETWORK_DISCONNECT: //网络断开 表示GPRS网络不可用澹，无法进行数据连接，有可能可以打电话
            iot_debug_print("[17_ota] OPENAT_NETWORK_DISCONNECT");
            iot_os_sleep(500);
            break;
        case OPENAT_NETWORK_READY: //网络已连接 表示GPRS网络可用，可以进行链路激活
            iot_debug_print("[17_ota] OPENAT_NETWORK_READY");
            memcpy(networkparam.apn, "CMNET", strlen("CMNET"));
            //建立网络连接，实际为pdp激活流程
            iot_network_connect(&networkparam);
            iot_os_sleep(500);
            break;
        case OPENAT_NETWORK_LINKED: //链路已经激活 PDP已经激活，可以通过socket接口建立数据连接
            iot_debug_print("[17_ota] OPENAT_NETWORK_LINKED");
            NetLink = TRUE;
            break;
        }
    }
    if (NetLink == TRUE)
    {
        if (OTA_Start() == true)
            iot_debug_print("[17_ota] OTA_Start OK");
        else
            iot_debug_print("[17_ota] OTA_Start false");

        while (1)
        {
            iot_debug_print("[17_ota] CSDK_VER :%s", CSDK_VER);
            iot_os_sleep(2000);
        }
    }
    iot_os_delete_task(TestTask_HANDLE);
}
static void NetWorkCb(E_OPENAT_NETWORK_STATE state)
{
    NetWorkCbMessage = state;
}
//main函数
int appimg_enter(void *param)
{
	//注册网络状态回调函数
	iot_network_set_cb(NetWorkCb);
    //关闭看门狗，死机不会重启。默认打开
    iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
    //打开调试信息，默认关闭
    iot_vat_send_cmd("AT^TRACECTRL=0,1,3\r\n", sizeof("AT^TRACECTRL=0,1,3\r\n"));
    iot_debug_print("[hello]appimg_enter");

    //创建一个任务
    TestTask_HANDLE = iot_os_create_task(TestTask, NULL, 4096, 10, OPENAT_OS_CREATE_DEFAULT, "TestTask");
    return 0;
}

//退出提示
void appimg_exit(void)
{
    OSI_LOGI(0, "application image exit");
}
