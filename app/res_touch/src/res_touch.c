
#include "res_touch.h"


#define res_touch_print //iot_debug_print

typedef struct _tp_param
{
    float xfac;
    float yfac;
    short xoff;
    short yoff;
} tp_param;

typedef struct _tp_pix
{
    u16 x;
    u16 y;
} tp_pix;

static tp_pix tp_pixad = {0};        //当前触控坐标的AD值,前触控坐标的像素值
static tp_param tp_param_data = {0}; //校准参数值

void TP_Write_Byte(u8 num)
{
    u8 count = 0;
    for (count = 0; count < 8; count++)
    {
        if (num & 0x80)
            DOUT(1);
        else
            DOUT(0);
        num <<= 1;
        TCLK(0);
        osiDelayUS(1);
        TCLK(1); //上升沿有效
        osiDelayUS(1);
    }
}

u16 ADS_Read_AD(u8 CMD)
{
    u8 count = 0;
    u16 Num = 0;
    TCLK(0);            //先拉低时钟
    DOUT(0);            //拉低数据线
    TCS(0);             //选中触摸屏IC
    TP_Write_Byte(CMD); //发送命令字
    osiDelayUS(3);      //ADS7846的转换时间最长为6us
    TCLK(0);
    osiDelayUS(1);
    TCLK(1); //给1个时钟，清除BUSY
    osiDelayUS(1);
    TCLK(0);
    for (u8 ab = 0; ab < 2; ab++)
    {
        for (count = 0; count < 8; count++) //读出16位数据,只有高12位有效
        {
            Num <<= 1;
            TCLK(0); //下降沿有效
            osiDelayUS(1);
            TCLK(1);
            osiDelayUS(1);

            if (TDIN)
                Num++;
        }
        //osiDelayUS(5);
    }
    Num >>= 4; //只有高12位有效.
    TCS(1);    //释放片选
    return (Num);
    //#endif
}

// u16 ADS_Read_AD(unsigned char CMD)
// {
//     UINT32 len = iot_spi_write(OPENAT_SPI_1, &CMD, 1);
//     UINT8 buf[2] = {0};
//     iot_spi_read(OPENAT_SPI_1, buf, 2);
//     return ((buf[0] << 8 | buf[1]) >> 3);
// }

//读取一个坐标值
//连续读取READ_TIMES次数据,对这些数据升序排列,
//然后去掉最低和最高LOST_VAL个数,取平均值
u16 ADS_Read_XY(u8 cmd)
{
#define READ_TIMES 5 //读取次数
#define LOST_VAL 1   //丢弃值
    u16 buf[READ_TIMES];

    for (u16 i = 0; i < READ_TIMES; i++)
    {
        buf[i] = ADS_Read_AD(cmd);
    }
    res_touch_print("[res_touch] ADS_Read_XY buf[0-7]:0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7]);
    res_touch_print("[res_touch] ADS_Read_XY buf[8-14]:0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x", buf[8], buf[9], buf[10], buf[11], buf[12], buf[13], buf[14]);

    for (u16 i = 0; i < READ_TIMES - 1; i++) //排序
    {
        for (u16 j = i + 1; j < READ_TIMES; j++)
        {
            if (buf[i] > buf[j]) //升序排列
            {
                u16 temp;
                temp = buf[i];
                buf[i] = buf[j];
                buf[j] = temp;
            }
        }
    }
    u16 sum = 0;
    for (u16 i = LOST_VAL; i < READ_TIMES - LOST_VAL; i++)
        sum += buf[i];
    u16 temp = sum / (READ_TIMES - 2 * LOST_VAL);

    res_touch_print("[res_touch] ADS_Read_XY cmd:0x%x temp:0x%x", cmd, temp);
    return temp;
}
//带滤波的坐标读取
//最小值不能少于100.
BOOL Read_ADS(u16 *x, u16 *y)
{
    u16 xtemp, ytemp;
    xtemp = ADS_Read_XY(CMD_RDX);
    ytemp = ADS_Read_XY(CMD_RDY);
    if (xtemp < 100 || ytemp < 100)
        return FALSE; //读数失败
    *x = xtemp;
    *y = ytemp;
    return TRUE; //读数成功
}
//2次读取ADS7846,连续读取2次有效的AD值,且这两次的偏差不能超过
//50,满足条件,则认为读数正确,否则读数错误.
//该函数能大大提高准确度
BOOL Read_ADS2(u16 *x, u16 *y, u8 errRange)
{
    u16 x1, y1;
    u16 x2, y2;
    if (!Read_ADS(&x1, &y1))
        return FALSE;
    if (!Read_ADS(&x2, &y2))
        return FALSE;

    if (((x2 <= x1 && x1 < x2 + errRange) || (x1 <= x2 && x2 < x1 + errRange)) //前后两次采样在+-50内
        && ((y2 <= y1 && y1 < y2 + errRange) || (y1 <= y2 && y2 < y1 + errRange)))
    {
        *x = (x1 + x2) >> 1;
        *y = (y1 + y2) >> 1;
        return TRUE;
    }
    else
        return FALSE;
}

// //精确读取一次坐标,校准的时候用的
// BOOL Read_TP_Once(void)
// {

//     if (!Read_ADS2(&tp_pixad.x, &tp_pixad.y))
//         return FALSE;
//     u16 x1, y1;
//     if (!Read_ADS2(&x1, &y1))
//         return FALSE;
//     if (tp_pixad.x == x1 && tp_pixad.y == y1)
//     {
//         return TRUE;
//     }
//     return FALSE;
// }

#include "lcd.h"

//画一个校准用的辅助触摸区域
void Drow_Touch_Point(u16 x, u16 y)
{
    LCD_DrawLine(x - 12, y, x + 13, y); //横线
    LCD_DrawLine(x, y - 12, x, y + 13); //竖线
    LCD_DrawPoint(x + 1, y + 1, 1);
    LCD_DrawPoint(x - 1, y + 1, 1);
    LCD_DrawPoint(x + 1, y - 1, 1);
    LCD_DrawPoint(x - 1, y - 1, 1);
    LCD_Draw_Circle(x, y, 6); //画中心圈
}

#include "iot_fs.h"

#define TP_PARAM_FILE_NAME "/tp_param_data.bin"

BOOL TP_Read_Adjdata(void)
{

    INT32 filehandle = iot_fs_open_file(TP_PARAM_FILE_NAME, FS_O_RDWR);
    if (filehandle < 0)
    {
        res_touch_print("[res_touch] iot_fs_open_file false err:%d", filehandle);
        return FALSE;
    }

    INT32 result = iot_fs_read_file(filehandle, (UINT8 *)&tp_param_data, sizeof(tp_param_data));
    if (result < 0)
    {
        res_touch_print("[res_touch] iot_fs_write_file false err:%d", result);
        iot_fs_close_file(filehandle);
        return FALSE;
    }
    else if (result < sizeof(tp_param_data))
    {
        res_touch_print("[res_touch] iot_fs_write_file result:%d < sizeof(tp_param_data):%d", result, sizeof(tp_param_data));
        iot_fs_close_file(filehandle);
        return FALSE;
    }
    iot_fs_close_file(filehandle);
    res_touch_print("[res_touch] TP_Read_Adjdata xfac:%f xoff:%d yfac:%f yoff:%d", tp_param_data.xfac, tp_param_data.xoff, tp_param_data.yfac, tp_param_data.yoff);
    return TRUE;
}

BOOL TP_Save_Adjdata(tp_pix *pos_temp)
{
    APP_LCD_DATA *handle = LCD_GET_SYS_DATA();
    //计算结果
    tp_param_data.xfac = (float)(handle->lcd_width - 40) / (pos_temp[1].x - pos_temp[0].x);                       //得到xfac
    tp_param_data.xoff = (short)(handle->lcd_width - (tp_param_data.xfac * (pos_temp[1].x + pos_temp[0].x))) / 2; //得到xoff

    tp_param_data.yfac = (float)(handle->lcd_heigh - 40) / (pos_temp[2].y - pos_temp[0].y);                       //得到yfac
    tp_param_data.yoff = (short)(handle->lcd_heigh - (tp_param_data.yfac * (pos_temp[2].y + pos_temp[0].y))) / 2; //得到yoff

    res_touch_print("[res_touch] TP_Save_Adjdata xfac:%f xoff:%d yfac:%f yoff:%d", tp_param_data.xfac, tp_param_data.xoff, tp_param_data.yfac, tp_param_data.yoff);

    INT32 filehandle = iot_fs_create_file(TP_PARAM_FILE_NAME);
    if (filehandle >= 0)
        iot_fs_close_file(filehandle);

    filehandle = iot_fs_open_file(TP_PARAM_FILE_NAME, FS_O_RDWR);
    if (filehandle < 0)
    {
        res_touch_print("[res_touch] iot_fs_open_file false err:%d", filehandle);
        return FALSE;
    }

    INT32 result = iot_fs_write_file(filehandle, (UINT8 *)&tp_param_data, sizeof(tp_param_data));
    if (result < 0)
    {
        res_touch_print("[res_touch] iot_fs_write_file false err:%d", result);
        iot_fs_close_file(filehandle);
        return FALSE;
    }
    iot_fs_close_file(filehandle);
    return TRUE;
}

BOOL TP_Read_Screen_XY(u16 *x, u16 *y)
{
    UINT8 Penirq = 0;
    iot_gpio_read(23, &Penirq);
    if (Penirq == 0) //按键按下了
    {
        u16 x1, y1;
        if (!Read_ADS2(&x1, &y1, 50))
            return FALSE;
        float x2 = tp_param_data.xfac * x1 + tp_param_data.xoff; //将结果转换为屏幕坐标
        float y2 = tp_param_data.yfac * y1 + tp_param_data.yoff;

        APP_LCD_DATA *handle = LCD_GET_SYS_DATA();
        if (x2 > handle->lcd_width || y2 > handle->lcd_heigh)
            return FALSE;

        *x = (u16)x2;
        *y = (u16)y2;
        return TRUE;
    }
    return FALSE;
}

BOOL Touch_Point_Switch(u8 cnt, tp_pix *pos_temp)
{
    APP_LCD_DATA *handle = LCD_GET_SYS_DATA();
#define Touch_Point_Position 20
    BOOL flag = FALSE;
    LCD_Clear();                                                                       //清屏
    LCD_ShowString(0, 40, "Please use the stylus click ", 16); //显示提示信息
    LCD_ShowString(0, 56, "the cross on the screen.", 16);    //显示提示信息
    LCD_ShowString(0, 72, "The cross will always moveuntil ", 16); //显示提示信息
    LCD_ShowString(0, 88, "the screen adjustment ", 16); //显示提示信息
    LCD_ShowString(0, 104, "is completed.", 16);              //显示提示信息
    switch (cnt)
    {
    case 0:
        Drow_Touch_Point(Touch_Point_Position, Touch_Point_Position); //画点2
        flag = TRUE;
        break;
    case 1:
        Drow_Touch_Point(handle->lcd_width - Touch_Point_Position, Touch_Point_Position); //画点2
        flag = TRUE;
        break;
    case 2:
        Drow_Touch_Point(Touch_Point_Position, handle->lcd_heigh - Touch_Point_Position); //画点3
        flag = TRUE;
        break;
    case 3:
        Drow_Touch_Point(handle->lcd_width - Touch_Point_Position, handle->lcd_heigh - Touch_Point_Position); //画点4
        flag = TRUE;
        break;
    case 4: //全部四个点已经得到
    {
        { //判断1、2之间的距离和3、4之间的距离是不是差不多
            u32 tem1, tem2;
            tem1 = abs(pos_temp[0].x - pos_temp[1].x); //x1-x2
            tem2 = abs(pos_temp[0].y - pos_temp[1].y); //y1-y2
            tem1 *= tem1;
            tem2 *= tem2;
            u16 d1 = sqrt(tem1 + tem2);                //得到1,2的距离
            tem1 = abs(pos_temp[2].x - pos_temp[3].x); //x3-x4
            tem2 = abs(pos_temp[2].y - pos_temp[3].y); //y3-y4
            tem1 *= tem1;
            tem2 *= tem2;
            u16 d2 = sqrt(tem1 + tem2); //得到3,4的距离
            double fac = (float)d1 / d2;
            res_touch_print("[res_touch] Touch_Point_Switch 12~34 fac:%f d1:%d d2:%d", fac, d1, d2);
            if (fac < 0.95 || fac > 1.05 || d1 == 0 || d2 == 0) //不合格
            {

                flag = FALSE;
                break;
            }
        }

        { //判断1、3之间的距离和2、4之间的距离是不是差不多
            u32 tem1, tem2;
            tem1 = abs(pos_temp[0].x - pos_temp[2].x); //x1-x3
            tem2 = abs(pos_temp[0].y - pos_temp[2].y); //y1-y3
            tem1 *= tem1;
            tem2 *= tem2;
            u16 d1 = sqrt(tem1 + tem2);                //得到1,3的距离
            tem1 = abs(pos_temp[1].x - pos_temp[3].x); //x2-x4
            tem2 = abs(pos_temp[1].y - pos_temp[3].y); //y2-y4
            tem1 *= tem1;
            tem2 *= tem2;
            u16 d2 = sqrt(tem1 + tem2); //得到2,4的距离
            double fac = (float)d1 / d2;
            res_touch_print("[res_touch] Touch_Point_Switch 13~24 fac:%f d1:%d d2:%d", fac, d1, d2);
            if (fac < 0.95 || fac > 1.05) //不合格
            {
                flag = FALSE;
                break;
            } //正确了
        }

        { //判断1、4之间的距离和2、3之间的距离是不是差不多
            u32 tem1, tem2;
            //对角线相等
            tem1 = abs(pos_temp[1].x - pos_temp[2].x); //x1-x3
            tem2 = abs(pos_temp[1].y - pos_temp[2].y); //y1-y3
            tem1 *= tem1;
            tem2 *= tem2;
            u16 d1 = sqrt(tem1 + tem2);                //得到1,4的距离
            tem1 = abs(pos_temp[0].x - pos_temp[3].x); //x2-x4
            tem2 = abs(pos_temp[0].y - pos_temp[3].y); //y2-y4
            tem1 *= tem1;
            tem2 *= tem2;
            u16 d2 = sqrt(tem1 + tem2); //得到2,3的距离
            double fac = (float)d1 / d2;
            res_touch_print("[res_touch] Touch_Point_Switch 14~23 fac:%f d1:%d d2:%d", fac, d1, d2);
            if (fac < 0.95 || fac > 1.05) //不合格
            {
                flag = FALSE;
                break;
            } //正确了
        }

        if (TP_Save_Adjdata(pos_temp) == FALSE)
            return FALSE;

        LCD_Clear();
        LCD_ShowString(35, 150, "Touch Screen Adjust OK!", 16); //校正完成
        flag = TRUE;
        break;
    }
    }
    LCD_Refresh_Gram();
    iot_os_sleep(1000);
    return flag;
}

BOOL Touch_Adjust(void)
{
    tp_pix p[4];
    u8 cnt = 0;
    UINT8 Penirq = 0;
    Touch_Point_Switch(cnt, p);
    while (1)
    {
        iot_gpio_read(23, &Penirq);
        if (Penirq == 0) //按键按下了
        {
            if (Read_ADS2(&(tp_pixad.x), &(tp_pixad.y), 50)) //得到单次按键值
            {

                p[cnt].x = tp_pixad.x;
                p[cnt].y = tp_pixad.y;
                cnt++;
                res_touch_print("[res_touch] Read_TP_Once OK cnt:%d tp_pixad.x:%d tp_pixad.y:%d", cnt, tp_pixad.x, tp_pixad.y);
                do
                {
                    iot_gpio_read(23, &Penirq);
                    iot_os_sleep(10);
                } while (Penirq == 0);
                if (Touch_Point_Switch(cnt, p) == FALSE)
                {
                    res_touch_print("[res_touch] Touch_Adjust FALSE");
                    return FALSE;
                }
                else
                {
                    if (cnt == 4)
                        return TRUE;
                }
            }
        }
        iot_os_sleep(100);
    }
}

// void Touch_handle(E_OPENAT_DRV_EVT evt, E_AMOPENAT_GPIO_PORT gpioPort, unsigned char state)
// {
//     uint8 status = 0;
//     // 判断是gpio中断
//     if (evt == OPENAT_DRV_EVT_GPIO_INT_IND)
//     {
//         // 判断触发中断的管脚
//         if (gpioPort == 23)
//         {
//             // 触发电平的状态
//             res_touch_print("[gpio] input handle gpio %d, state %d", gpioPort, state);

//         }
//     }
// }

void Touch_init(void)
{
    // T_AMOPENAT_SPI_PARAM cfg = {
    //     .fullDuplex = 0,
    //     .cpol = 1,
    //     .cpha = 1,
    //     .dataBits = 8,
    //     .clock = 110000,
    //     .withCS = 0};
    // iot_spi_open(OPENAT_SPI_1, &cfg);

    T_AMOPENAT_GPIO_CFG output_cfg = {
        .mode = OPENAT_GPIO_OUTPUT,
        .param.defaultState = 1};

    T_AMOPENAT_GPIO_CFG input_cfg = {
        .mode = OPENAT_GPIO_INPUT_INT,
        .param.defaultState = 1,
        .param.pullState = OPENAT_GPIO_PULLUP,
        .param.intCfg.debounce = 0};

    iot_gpio_open(9, &output_cfg);
    iot_gpio_open(10, &output_cfg);
    iot_gpio_open(11, &output_cfg);
    iot_gpio_open(12, &input_cfg);

    iot_gpio_open(23, &input_cfg);

    Read_ADS2(&(tp_pixad.x), &(tp_pixad.y), 50);
}