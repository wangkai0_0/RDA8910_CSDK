
#include "driver_comm.h"

#undef DRV_NAME
#define DRV_NAME "DRV_MAIN"


int appimg_enter(void *param)
{    
	DRV_PRINT("[%s,%d]",__FUNCTION__,__LINE__);

	iot_debug_set_fault_mode(OPENAT_FAULT_HANG);

	adcTest();

	uartTest();

	i2cTest();

	keyPadTest();

	spiTest();

	lcdTest();

	cmaeraTest();

	pwmTest();

	sdTest();
	
    return 0;
}

void appimg_exit(void)
{
    DRV_PRINT("[%s,%d]",__FUNCTION__,__LINE__);
}

